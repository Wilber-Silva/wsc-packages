# Types #

O pacote Types é onde ficará todas todos os tipos e interfaces, sendo possível compartilhar um contrato ou um tipo de dado entre back-end e front-end

Como todas as tipagens ficaram nesse pacote as mesmas seram organizadas por escopo ou contexto por exemplo todos as interfaces que foram criadas para o pacote **core** estão no path **/core** e o mesmo repete para os outros pacotes. Caso aconteça de ter um types generico que pode ser usado em todos os pacotes o mesmo deve ser inserido em uma pasta aparte

## Warning ##

Para não dar problema nas pontas que usam o types uma das premissas para editar este pacote é nunca editar uma interface, mas sim extende-la para uma nova interface
