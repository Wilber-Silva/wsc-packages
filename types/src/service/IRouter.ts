import { IRouter as IRouterExpress } from 'express'

export interface IRouteEvent {}
export type PathParams = string | RegExp | (string | RegExp)[]

export interface IRouter extends IRouterExpress {}
