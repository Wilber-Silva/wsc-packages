export const enum ValidationCrudTypes {
  create = 'create',
  createMany = 'createMany',
  update = 'update',
  updateMany = 'updateMany',
  delete = 'delete',
  deleteMany = 'deleteMany',
  find = 'find',
  get = 'get',
  list = 'list',
  findById = 'findById',
  findOne = 'findOne',
  getOne = 'getOne',
}

export interface ICrudValidation {
  create?: any
  createMany?: any
  update?: any
  updateMany?: any
  delete?: any
  deleteMany?: any
  find?: any
  get?: any
  list?: any
  findById?: any
  findOne?: any
  getOne?: any
}
