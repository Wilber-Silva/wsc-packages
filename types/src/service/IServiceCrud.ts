import { _id } from './../database'

export interface IServiceCrud {
  /**
   * Get a collection
   * @param query Data to make a query
   */
  get<T>(query: any): Promise<T[]>
  /**
   * Get a collection
   * @param query Data to make a query
   */
  list<T>(query: any): Promise<T[]>
  /**
   * Get a collection
   * @param query Data to make a query
   */
  find<T>(query: any): Promise<T[]>
  /**
   * Get item by reference
   * @param _id reference item
   */
  findById<T>(_id: _id): Promise<T>
  /**
   * Find one item
   * @param query Query to find item
   */
  findOne<T>(query: any): Promise<T>
  /**
   * Find one item
   * @param query Query to find item
   */
  getOne<T>(query: any): Promise<T>
  /**
   * Create a item
   * @param data data to save
   */
  create<T>(data: T): Promise<T>
  /**
   * Create many items
   * @param collectionData items to create
   */
  createMany<T>(collectionData: T[]): Promise<T[]>
  /**
   * Edit a item
   * @param _id reference item
   * @param data data to update
   */
  update<T>(_id: _id, data: T): Promise<T>
  /**
   * Edit many items
   * @param collectionData collection to update
   */
  updateMany<T>(collectionData: T[]): Promise<boolean>
  /**
   * Delete a item
   * @param id reference item
   */
  delete(id: _id): Promise<boolean>
  /**
   * Delete many items
   * @param ids collection of reference items
   */
  deleteMany(ids: _id[]): Promise<boolean>
}
