import * as http from 'http'
import { IApplication, IHandler, IMiddleware } from '..'

export interface IHandlerExpress extends IHandler {
  /**
   * Method to dispach daemon to port listen
   *
   * @param application Application instance
   * @return void
   */
  dispach(application: IApplication): void

  /**
   * Method to get Express instance
   *
   * @return http.Server
   */
  getInstance(): http.Server

  /**
   * Method to stop execution of handler
   *
   * @return void
   */
  stop(): void

  /**
   * Method to get args of Handler
   *
   * @return { [index: string]: string | number }
   */
  getArgs(): { [index: string]: string | number }

  /**
   * Method to get middlewares default
   *
   * @return IMiddleware[]
   */
  getMiddlewaresDefaut(): IMiddleware[]
}
