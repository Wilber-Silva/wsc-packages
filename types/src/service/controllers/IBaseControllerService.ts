import { ICrudValidation } from './../ICrudValidation'
import express from 'express'

export interface IBaseControllerService {
  /**
   * Label of this controller
   * @param string
   */
  label: string

  /**
   * Base path of controller
   * @param string
   */
  path: string

  /**
   * Routes of controller to Express
   *
   * @return any
   */
  router: express.Router
  /**
   * Method to init routes of controller
   * @return void
   */
  initRoutes(): void

  /**
   * Method to validate data
   * @return any
   */
  validationSchema(): ICrudValidation | any

  /**
   * Method to validate request
   *
   * @param validationSchema Schema to validate
   * @param input Data input to validate
   * @return Promise<any>
   */
  requestValidate(
    validationSchema: Record<string, any>,
    input: Record<string, any>,
  ): Promise<any>
}
