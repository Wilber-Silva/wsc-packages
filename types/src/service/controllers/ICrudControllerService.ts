import express from 'express'
import { IBaseControllerService } from './'

export interface ICrudControllerService extends IBaseControllerService {
  /**
   * Method to create record into Service
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<any>
   */
  create(request: express.Request, response: express.Response): Promise<any>

  /**
   * Method to create many records
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<string[]>
   */
  createMany(
    request: express.Request,
    response: express.Response,
  ): Promise<string[]>

  /**
   * Method to find one record by id
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<T>
   */
  find<T>(request: express.Request, response: express.Response): Promise<T>

  /**
   * Method to update record
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<void>
   */
  update(request: express.Request, response: express.Response): Promise<void>

  /**
   * Method to update record to deleted
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<void>
   */
  delete(request: express.Request, response: express.Response): Promise<void>
}
