export type date = Date | string

export interface IBaseSchema {
  _id?: string
  createdAt?: date
  updatedAt?: date
  deletedAt?: date
}
