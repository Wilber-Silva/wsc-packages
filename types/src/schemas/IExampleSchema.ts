import { IBaseSchema } from './IBaseSchema'

export interface IExampleSchema extends IBaseSchema {
  name: string
}
