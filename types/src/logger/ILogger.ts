export interface ILogger {
  /**
   * Another log settings
   */
  [name: string]: any
  /**
   * Method to log trace
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  trace(message: string, args?: any): void

  /**
   * Method to log debug
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  debug(message: string, args?: any): void

  /**
   * Method to log info
   *
   * @param message message to log
   * @return void
   */
  info(message: string, args?: any): void

  /**
   * Method to log warning
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  warning(message: string, args?: any): void

  /**
   * Method to log error
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  error(message: string, args?: any): void

  /**
   * Method to log exception
   *
   * @param err Exception to parse and log
   * @return void
   */
  exception(err: Error): void

  /**
   * Method to log fatal
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  fatal(message: string, args?: any): void
}
