import { IApplication, IMiddleware } from '.'

export interface IHandler {
  /**
   * Method for dispach request to parser, eg: Express or Google Functions
   *
   * @param application Instance of application to use in Handler
   * @return void
   */
  dispach(application: IApplication): void

  /**
   * Method to stop execution of handler
   *
   * @return void
   */
  stop(): void

  /**
   * Method to get args of handler
   *
   * @return { [index: string]: string | number }
   */
  getArgs(): { [index: string]: string | number }

  /**
   * Method to get middlewares default
   *
   * @return IMiddleware[]
   */
  getMiddlewaresDefaut?(): IMiddleware[]
}
