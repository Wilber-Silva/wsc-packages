import { IHandler } from '.'

export interface IApplication {
  /**
   * Name of application
   * @param string
   */
  name: string

  /**
   * Method to configure trigger after dispach
   *
   * @param callback Callback to call after dispach
   * @return void
   */
  afterDispach(callback: (application: IApplication) => boolean): void

  /**
   * Method to configure trigger before dispach
   *
   * @param callback Callback to call after dispach
   * @return void
   */
  beforeDispach(callback: (application: IApplication) => boolean): void

  /**
   * Method to dispach daemon to Express or Google Functions
   *
   * @throws Error on execute handler trigger after, before or dispach handler
   * @return void
   */
  dispach(): void

  /**
   * Method to stop execution of handler
   *
   * @return void
   */
  stop(): void

  /**
   * Method to get instance of Handler
   *
   * @return IHandler
   */
  getHandler(): IHandler
}
