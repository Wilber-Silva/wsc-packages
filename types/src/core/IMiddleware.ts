export interface IMiddleware {
  /**
   * Method to get a middleware
   *
   * @return any
   */
  get(): any
}
