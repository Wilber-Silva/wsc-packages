import { IDataBaseProvider } from './../database/IDataBaseProvider'
import { IBaseSchema } from './../schemas/IBaseSchema'
import { _id } from './../database'

export interface IDatabaseRepository {
  /**
   * Database provider instance
   */
  _provider: IDataBaseProvider
  /**
   * Options to get a collection
   * @param data
   * @returns Collections off data
   */
  find<T extends IBaseSchema>(data: T): Promise<T[]>
  /**
   * Options to get a collection
   * @param data
   * @returns Collections off data
   */
  list<T extends IBaseSchema>(data: T): Promise<T[]>
  /**
   * Options to get data
   * @param options
   * @returns Only one line of data
   */
  findOne<T extends IBaseSchema>(data: T): Promise<T>
  /**
   * Data object find one or create this if not exists
   * @param data
   * @returns Data
   */
  findOrCreate<T extends IBaseSchema>(data: T): Promise<T>
  /**
   * Id of line
   * @param _id
   * @returns One data of line
   */
  findById<T extends IBaseSchema>(_id: _id): Promise<T>
  /**
   * Data to create
   * @param data
   * @returns Data with id
   */
  create<T extends IBaseSchema>(data: T): Promise<T>
  /**
   * Collection to create
   * @param collectionToCreate
   * @returns All data with _id
   */
  createMany<T extends IBaseSchema>(collectionToCreate: T[]): Promise<T[]>
  /**
   * Id of line
   * @param _id
   * Data to update
   * @param data
   * @returns Data updated
   */
  update<T extends IBaseSchema>(_id: _id, data: T): Promise<T>
  /**
   * Collection to update
   * @param collectionToUpdate
   * @returns Collection of data with a flag
   */
  updateMany<T extends IBaseSchema>(data: T[], options?: any): Promise<boolean>

  /**
   * Id of data line
   * @param _id
   * Force to delete forever or continue with a logic deleted
   * @param force
   * @returns True if is success deleted
   */
  delete(_id: _id, force?: boolean): Promise<boolean>
  /**
   * Ids of lines to delete
   * @param ids
   * Force to forever delete
   * @param force
   * @returns Collection data with flag
   */
  deleteMany(ids: _id[], force?: boolean): Promise<boolean>
}
