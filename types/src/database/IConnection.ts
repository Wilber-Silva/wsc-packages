export interface IConnection {
  /**
   * Create connection
   */
  connect(): Promise<void>
  /**
   * Close connection
   */
  close(): Promise<void>
}
