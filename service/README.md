# Service #

O Pacote **Service** é voltado para criar aplicações HTTP

## BaseController ##

O base controller já vem com alguns genericos metodos prontos para crud

Como definido em **types** todos os controllers devem o initRoutes, validationSchema, requestValidate que já são pré-implementados no base

## HTTP Status ##

**Http Status** é um enum com os status HTTP que podemos usar em diferentes situações chamando por um nome constante e não por um número hard-code

~~~javascript
import { HttpStatus } from '@wsc-company/service'

function(code: HttpStatus): boolean {
    return code === HttpStatus.OK
}

~~~

## Examples ##

* Controller

~~~javascript
import { BaseController } from '@wsc-company/service'
import { IController, ILogger } from '@wsc-company/types'
import { Router } from 'express'
import * as Joi from 'joi'
import { Factory as FactoryService } from './../../Services'

export class ExampleController extends BaseController implements IController {
  /**
   * Logger to log application
   * @param ILogger
   */
  protected _logger: ILogger

  /**
   * Method to construct instance of Users Controller
   *
   * @param _logger Instance of logger
   * @param expressRouter Instance of Express Router
   */
  constructor(
    /**
     * Logger to log application
     * @param ILogger
     */
    logger: ILogger,

    /**
     * Instance of Express Router
     * @param Router
     */
    expressRouter: Router,
  ) {
    super(
      logger, // logger instance
      expressRouter, // Router instance
      'Example', // Controller Name
      '/example', // HTTP uri
      FactoryService.getGroupsService(), // Controller Service
      null, // Controller Repository
    )
    this._logger = logger
    this.initRoutes()
  }

  public initRoutes = (): void => {
    // Route example
    this.router.post(this.path, this.create.bind(this))
  }

  public validationSchema = (): any => {
    const create = Joi.object().keys({
      name: Joi.string().required(),
    })
    return {
      create
    }
  }
}
~~~
