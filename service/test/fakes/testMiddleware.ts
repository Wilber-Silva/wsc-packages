import { IMiddleware } from '@wsc-company/types'
import * as bodyParser from 'body-parser'
import { RequestHandler } from 'express'

export class TestMiddleware implements IMiddleware {
  public hasConstructor = false

  constructor() {
    this.hasConstructor = true
  }
  public get = (): RequestHandler => {
    return bodyParser.json()
  }
}
