/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import { ILogger } from '@wsc-company/types'
// tslint:disable-next-line: class-name
export class Logger implements ILogger {
  trace(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
  debug(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
  info(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
  warning(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
  error(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
  exception(err: Error): void {
    // throw new Error('Method not implemented.');
  }
  fatal(message: string, args?: any): void {
    // throw new Error('Method not implemented.');
  }
}
