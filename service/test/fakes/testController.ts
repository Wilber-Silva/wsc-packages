/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { IController } from '@wsc-company/types'
import { Router, Request, Response } from 'express'

export class TestController implements IController {
  public label: string = null
  public path: string = null
  public router: Router = null
  public hasConstructor = false

  constructor() {
    this.router = Router()
    this.hasConstructor = true
  }
  public initRoutes = (): void => {}
  public validationSchema = (): any => {}
  public requestValidate = async (): Promise<any> => {}
  public create = async (
    validationSchema: Record<string, any>,
    input: Record<string, any>,
  ): Promise<any> => {}

  public createMany = async (
    request: Request,
    response: Response,
  ): Promise<string[]> => {
    return []
  }

  public find = async <T>(request: Request, response: Response): Promise<T> => {
    return null
  }

  public update = async (
    request: Request,
    response: Response,
  ): Promise<void> => {
    return null
  }

  public delete = async (
    request: Request,
    response: Response,
  ): Promise<void> => {}
}
