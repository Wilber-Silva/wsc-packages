/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
export class TestService {
  create(body: any): Promise<string> {
    console.debug('Service create body')
    return Promise.resolve('fake-id')
  }
  createMany(body: any): Promise<string[]> {
    console.debug('Service create many body')
    return Promise.resolve(['fake-id', 'fake-id'])
  }
  update(id: string, body: any): Promise<boolean> {
    console.debug('Service update id')
    console.debug('Service update body')
    return Promise.resolve(true)
  }
}
