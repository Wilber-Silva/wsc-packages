import { IApplication, IHandlerExpress, ILogger } from '@wsc-company/types'
import * as http from 'http'
import { HandlerExpress } from '../../src'
import { TestController, TestMiddleware, Logger } from './../fakes'
import * as express from 'express'

describe('Handler Express Test', () => {
  const applicationPort = 9999
  const application: IApplication = {
    name: 'application test',
    afterDispach: null,
    beforeDispach: null,
    dispach: () => null,
    getHandler: () => null,
    stop: () => null,
  }
  const logger = new Logger()

  let handlerExpress: IHandlerExpress = null
  let controller: TestController = null
  let middleware: TestMiddleware = null

  beforeEach(() => {
    controller = new TestController()
    middleware = new TestMiddleware()
    handlerExpress = new HandlerExpress(
      logger,
      express(),
      { port: applicationPort },
      [],
      [],
    )
    // handlerExpress = Factory.getHandlerExpress(applicationPort, [middleware], [controller]);
  })

  // 'Test dispach of Handler Express'
  it('Test dispach of Handler Express', () => {
    handlerExpress.dispach(application)
    expect(handlerExpress.getInstance()).toBeInstanceOf(http.Server)
    handlerExpress.stop()
  })

  // 'Test middleware constructor'
  it('Test middleware constructor', () => {
    handlerExpress.dispach(application)
    expect(middleware.hasConstructor).toBe(true)
    handlerExpress.stop()
  })

  // 'Test controller constructor'
  it('Test controller constructor', () => {
    handlerExpress.dispach(application)
    expect(controller.hasConstructor).toBe(true)
    handlerExpress.stop()
  })

  // Test exception without args port
  it('Test exception without args port', () => {
    let hasException = false
    const localHandlerExpress = new HandlerExpress(
      logger,
      express(),
      { port: null },
      [middleware],
      [controller],
    )
    // const localHandlerExpress = Factory.getHandlerExpress(null, [middleware], [controller]);
    try {
      localHandlerExpress.dispach(application)
    } catch (err) {
      hasException = true
    } finally {
      localHandlerExpress.stop()
      expect(hasException).toBe(true)
    }
  })

  // Test to get args
  it('Test to get args port', () => {
    handlerExpress.dispach(application)
    expect(handlerExpress.getArgs().port).toBe(applicationPort)
    handlerExpress.stop()
  })

  // todo: test listen express method
})
