import { HttpException, HttpStatus } from './../../src'

describe('Test Http Exception', () => {
  let success: HttpException = null
  let badRequest: HttpException = null
  let internalError: HttpException = null

  const successMessage = 'success'
  const badRequestMessage = 'bad-request'
  const internalErrorMessage = 'internal-error'

  beforeEach(() => {
    success = new HttpException(HttpStatus.OK, successMessage)
    badRequest = new HttpException(HttpStatus.BAD_REQUEST, badRequestMessage)
    internalError = new HttpException(
      HttpStatus.INTERNAL_SERVER_ERROR,
      internalErrorMessage,
    )
  })

  it('Success', () => {
    expect(success.statusCode).toBe(HttpStatus.OK)
    expect(success.message).toBe(successMessage)
  })

  it('Bad Request', () => {
    expect(badRequest.statusCode).toBe(HttpStatus.BAD_REQUEST)
    expect(badRequest.message).toBe(badRequestMessage)
  })

  it('Internal Server Error', () => {
    expect(internalError.statusCode).toBe(HttpStatus.INTERNAL_SERVER_ERROR)
    expect(internalError.message).toBe(internalErrorMessage)
  })
})
