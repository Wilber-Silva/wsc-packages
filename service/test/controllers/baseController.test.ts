import { ICrudControllerService } from '@wsc-company/types'
import { TestController } from './../fakes/testController'
import * as Joi from 'joi'

describe('Base Controller Test', () => {
  let controller: ICrudControllerService = null

  beforeEach(() => {
    controller = new TestController()
  })

  it('Test Controller', () => {
    expect(controller).toBeDefined()
  })

  it('requestValidate', () => {
    controller.requestValidate(
      Joi.object().keys({
        name: Joi.string().required(),
      }),
      {
        name: 'Test',
      },
    )
  })
})
