import { IHandler, ILogger } from '@wsc-company/types'
import * as fexpress from 'express'
import {
  BodyParserJSONMiddleware,
  BodyParserUrlEncodedMiddleware,
  Factory,
  HandlerExpress,
} from '../src'

describe('Factory Test', () => {
  let handler: IHandler = null

  const logger: ILogger = {
    debug: () => null,
    error: () => null,
    exception: () => null,
    fatal: () => null,
    info: () => null,
    trace: () => null,
    warning: () => null,
  }

  beforeEach(() => {
    handler = Factory.getHandler(logger, fexpress(), [], [], {})
  })

  // Test get Handler instance
  it('Test get Handler instance', () => {
    expect(handler).toBeInstanceOf(HandlerExpress)
  })

  // Test get Middleware Body Parser JSON instance
  it('Test get Middleware Body Parser JSON instance', () => {
    const _handler = Factory.getMiddlewareBodyParserJSON()
    expect(_handler).toBeInstanceOf(BodyParserJSONMiddleware)
  })

  // Test get Middleware Body Parser Url Encoded instance
  it('Test get Middleware Body Parser Url Encoded instance', () => {
    const _handler = Factory.getMiddlewareBodyParserUrlEncoded()
    expect(_handler).toBeInstanceOf(BodyParserUrlEncodedMiddleware)
  })
})
