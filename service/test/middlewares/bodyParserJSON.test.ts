import { IMiddleware } from '@wsc-company/types'
import { BodyParserJSONMiddleware } from '../../src'

describe('Body Parser JSON Test', () => {
  let middleware: IMiddleware = null

  beforeEach(() => {
    middleware = new BodyParserJSONMiddleware()
  })

  // 'Test get method'
  it('Test get method', () => {
    expect(middleware.get().constructor.name).toBe('Function')
  })
})
