import { IMiddleware } from '@wsc-company/types'
import {
  BodyParserJSONMiddleware,
  BodyParserUrlEncodedMiddleware,
} from '../../src'

describe('Body Parser Url Encoded Test', () => {
  let middleware: IMiddleware = null

  beforeEach(() => {
    middleware = new BodyParserUrlEncodedMiddleware()
  })

  // 'Test get method'
  it('Test get method', () => {
    expect(middleware.get().constructor.name).toBe('Function')
  })
})
