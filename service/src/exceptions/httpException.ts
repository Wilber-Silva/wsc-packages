export class HttpException extends Error {
  public readonly statusCode: number
  public readonly message: string
  constructor(statusCode: number, response: string | Record<string, any>) {
    super()
    this.statusCode = statusCode

    if (typeof response === 'string') {
      this.message = response
    } else if (
      typeof response === 'object' &&
      typeof response.message === 'string'
    ) {
      this.message = response.message
    } else if (this.constructor) {
      this.message = this.constructor.name
        .match(/[A-Z][a-z]+|[0-9]+/g)
        .join(' ')
    }
  }
}
