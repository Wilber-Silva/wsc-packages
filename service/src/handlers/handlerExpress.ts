import {
  IApplication,
  IController,
  IHandlerExpress,
  ILogger,
  IMiddleware,
} from '@wsc-company/types'
import express from 'express'
import * as http from 'http'
import { Factory } from '..'

export class HandlerExpress implements IHandlerExpress {
  /**
   * Instance of application
   * @param IApplication
   */
  private _application: IApplication = null

  /**
   * Instance of Server
   * @param http.Server
   */
  private _instance: http.Server = null

  /**
   * Method to construct instance of HandlerExpress
   *
   * @param _logger Logger to log application
   * @param _express Express framework to create routes
   * @param _httpPort Port to execute HTTP server
   * @param _middlewares Middlewares to apply on execute controller in application
   * @param _controllers Controllers to enable in this application
   * @return void
   */
  constructor(
    /**
     * Instance of logger
     * @param ILogger
     */
    private _logger: ILogger,

    /**
     * Instance of Express
     * @param express.Application
     */
    private _express: express.Application,

    /**
     * Arguments to Handle Express
     * @param { [index: string]: string | number }
     */
    private _args: { [index: string]: string | number },

    /**
     * List of middlewares to apply in Express
     * @param IMiddleware[]
     */
    private _middlewares: IMiddleware[],

    /**
     * List of controllers to enable in Express
     * @param IController[]
     */
    private _controllers: IController[],
  ) {
    this.middlewares()
    this.routes()
  }

  /**
   * Method to dispach daemon to port listen
   *
   * @param application Application instance
   * @return void
   */
  public dispach = (application: IApplication): void => {
    this._application = application
    if (this._args && this._args.port) {
      this._logger.info('try listen application', {
        application: application.name,
        port: this._args.port,
      })
      this._instance = this._express.listen(this._args.port, () => {
        this._logger.debug('Success on listen application', {
          application: application.name,
          port: this._args.port,
        })
      })
    } else {
      this._logger.error('This app not have port on args!', {
        application: application.name,
        args: this._args,
      })
      throw Error(
        `Start application has error! Not port to configuration Express`,
      )
    }
  }

  /**
   * Method to get Express instance
   *
   * @return http.Server
   */
  public getInstance = (): http.Server => {
    return this._instance
  }

  /**
   * Method to stop execution of handler
   *
   * @return void
   */
  public stop = (): void => {
    if (this._instance) {
      this._logger.info('stop listen application', {
        application: this._application.name,
        port: this._args.port,
      })
      this._instance.close()
    }
  }

  /**
   * Method to get args of Handler
   *
   * @return { [index: string]: string | number }
   */
  public getArgs = (): { [index: string]: string | number } => {
    return this._args
  }

  /**
   * Method to get middlewares default
   *
   * @return IMiddleware[]
   */
  public getMiddlewaresDefaut = (): IMiddleware[] => {
    return [
      Factory.getMiddlewareBodyParserJSON(),
      Factory.getMiddlewareBodyParserUrlEncoded(),
      Factory.getMiddlewareLogger(this._logger),
    ]
  }

  /**
   * Method to load middlewares of application
   *
   * @return void
   */
  private middlewares = (): void => {
    const middlewares = [...this._middlewares, ...this.getMiddlewaresDefaut()]
    this._logger.debug('Apply middlewares', {
      middlewares,
    })
    middlewares.map((middleware) => {
      this._express.use(middleware.get())
    })
  }

  /**
   * Method to load routes of controllers
   *
   * @return void
   */
  private routes = (): void => {
    this._logger.debug('Apply controllers routes', {
      controllers: this._controllers,
    })
    this._controllers.map((controller) => {
      this._express.use('/', controller.router)
    })
  }
}
