import { IMiddleware } from '@wsc-company/types'
import * as bodyParser from 'body-parser'
import { RequestHandler } from 'express'

export class BodyParserUrlEncodedMiddleware implements IMiddleware {
  /**
   * Method to get middleware to body parser url encoded
   *
   * @return RequestHandler
   */
  public get = (): RequestHandler => {
    return bodyParser.urlencoded({ extended: true })
  }
}
