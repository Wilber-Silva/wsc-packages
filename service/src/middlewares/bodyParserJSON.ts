import { IMiddleware } from '@wsc-company/types'
import * as bodyParser from 'body-parser'
import { RequestHandler } from 'express'

export class BodyParserJSONMiddleware implements IMiddleware {
  /**
   * Method to get middleware to body parser JSON
   *
   * @return RequestHandler
   */
  public get = (): RequestHandler => {
    return bodyParser.json()
  }
}
