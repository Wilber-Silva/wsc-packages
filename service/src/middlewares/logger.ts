import { IMiddleware, ILogger } from '@wsc-company/types'
import express from 'express'

export class LoggerMiddleware implements IMiddleware {
  /**
   * Method to construct instance of Logger Middlare
   *
   * @param _logger Logger to log application
   * @return void
   */
  constructor(
    /**
     * Logger to log application
     * @param ILogger
     */
    private _logger: ILogger,
  ) {}

  /**
   * Method to get a middleware
   *
   * @return RequestHandler
   */
  public get = (): express.RequestHandler => {
    return (
      request: express.Request,
      response: express.Response,
      next: express.NextFunction,
    ) => {
      const { method, path, ip, hostname, headers, query, body } = request

      this._logger.info('Request received', { method, path, ip })
      this._logger.debug('Request debug', {
        method,
        path,
        ip,
        hostname,
        headers,
        query,
        body,
      })

      next()
    }
  }
}
