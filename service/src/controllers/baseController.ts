import {
  ICrudControllerService,
  ILogger,
  IRouter,
  IRequest,
  IResponse,
  IServiceCrud,
  ICrudValidation,
  ValidationCrudTypes,
} from '@wsc-company/types'
import { HttpException, HttpStatus } from '..'

export abstract class BaseController implements ICrudControllerService {
  /**
   * Method to construct class extends BaseController
   *
   * @param _logger Logger to log application
   * @param router Router to parse request
   * @param label Label of Controller
   * @param path Path root of Controller
   * @param _service Service to apply in this Controller
   */
  constructor(
    /**
     * Logger to log application
     * @param ILogger
     */
    protected _logger: ILogger,

    /**
     * Routes of controller
     * @param IRouter
     */
    public readonly router: IRouter,

    /**
     * Label of this controller
     * @param string
     */
    public label: string,

    /**
     * Root path of this service
     * @param string
     */
    public path: string,

    /**
     * Service to execute in data source
     * @param any
     */
    private _service?: IServiceCrud,
  ) {}
  /**
   * Method to init routes of controller
   *
   * @return void
   */
  public initRoutes = (): void => {
    // init Routes here
  }

  protected getSchema(schemaName: ValidationCrudTypes | string): any {
    const all = this.validationSchema()
    return all[schemaName]
  }

  /**
   * Method to validate data
   *
   * @return ICrudValidation | any
   */
  public validationSchema = (): ICrudValidation | any => {
    return null
  }

  /**
   * Method to validate request
   *
   * @param validator Schema to validate
   * @param input Data input to validate
   * @return Promise<any>
   */
  public requestValidate = async (
    validator: Record<string, any>,
    input: Record<string, any>,
  ): Promise<any> => {
    try {
      const result = await validator.validate(input)
      if (result.error) throw result.error
      return result
    } catch (e) {
      this._logger.error('Validation Error', e)
      throw new HttpException(HttpStatus.BAD_REQUEST, {
        message: e.message,
        details: {
          values: e._original,
          errors: e.details,
        },
      })
    }
  }

  /**
   * Method to create record into Service
   *
   * @param request Request of client
   * @param response Response to client
   * @param schema Schema of data to validate
   * @param service Service to use in create
   * @return Promise<any>
   */
  public async create(request: IRequest, response: IResponse): Promise<void> {
    try {
      const { body } = request
      const schema = this.getSchema('create')
      await this.requestValidate(schema, body)

      const created = await this._service.create(body)
      if (!created)
        throw new HttpException(
          HttpStatus.UNPROCESSABLE_ENTITY,
          `${this.label} Already exists`,
        )
      response.status(HttpStatus.CREATED).json({ _id: created._id })
      return created
    } catch (error) {
      console.error(error)
      response
        .status(error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR)
        .json(error)
    }
  }

  /**
   * Method to create many records
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<void>
   */
  public async createMany<T>(
    request: IRequest,
    response: IResponse,
  ): Promise<T[]> {
    try {
      const { body } = request
      const schema = this.getSchema('create')
      await this.requestValidate(schema, body)
      const createdIds = await this._service.createMany<T>(body)

      response.status(HttpStatus.OK).json({ _ids: createdIds })
      return createdIds
    } catch (e) {
      this._logger.exception(e)
      response.status(e.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).json(e)
    }
  }

  /**
   * Method to find one record by id
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<T>
   */
  public async find<T>(request: IRequest, response: IResponse): Promise<T> {
    try {
      const { id } = request.params
      const data = await this._service.findById<T>(String(id))
      if (!data) {
        response.status(HttpStatus.NOT_FOUND).json({})
      } else {
        response.status(HttpStatus.OK).json(data)
      }
      return data
    } catch (e) {
      this._logger.exception(e)
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e)
    }
  }

  /**
   * Method to update record
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<void>
   */
  public async update(request: IRequest, response: IResponse): Promise<void> {
    try {
      const { id } = request.params
      const { body } = request
      const schema = this.getSchema('create')

      await this.requestValidate(schema, body)
      const updated = await this._service.update(String(id), body)

      if (updated) response.status(HttpStatus.OK).json('')

      throw new HttpException(
        HttpStatus.NOT_MODIFIED,
        `${this.label} not modified`,
      )
    } catch (e) {
      this._logger.exception(e)
      response.status(e.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).json(e)
    }
  }

  /**
   * Method to list records
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<ant>
   */
  public async list<T>(request: IRequest, response: IResponse): Promise<T[]> {
    try {
      const { query } = request
      const schema = this.getSchema('create')
      await this.requestValidate(schema, query)
      const list = await this._service.list<T>(query)

      if (!list.length) {
        response.status(HttpStatus.NO_CONTENT).send()
      } else {
        response.status(HttpStatus.OK).json(list)
      }

      return list
    } catch (e) {
      this._logger.exception(e)
      response.status(e.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).json(e)
    }
  }

  /**
   * Method to update record to deleted
   *
   * @param request Request of client
   * @param response Response to client
   * @return Promise<void>
   */
  public async delete(request: IRequest, response: IResponse): Promise<void> {
    try {
      const { id } = request.params
      const deleted = await this._service.delete(String(id))
      response
        .status(deleted ? HttpStatus.OK : HttpStatus.UNPROCESSABLE_ENTITY)
        .json(deleted)
    } catch (e) {
      this._logger.exception(e)
      response.status(HttpStatus.INTERNAL_SERVER_ERROR).json(e)
    }
  }
}
