import { IController, IHandler, ILogger, IMiddleware } from '@wsc-company/types'
import express from 'express'
import {
  BodyParserJSONMiddleware,
  BodyParserUrlEncodedMiddleware,
  HandlerExpress,
  LoggerMiddleware,
} from '.'

export class Factory {
  /**
   * Method to get instance of handler express
   *
   * @param logger Logger to log application
   * @param express Express framework to parse routes
   * @param middlewares Middlewares to apply in Express
   * @param controllers Controllers to enable in Express
   * @param args Arguments to Express
   * @return IHandler
   */
  public static getHandler = (
    logger: ILogger,
    _express: express.Application,
    middlewares: IMiddleware[],
    controllers: IController[],
    args?: { [index: string]: string | number },
  ): IHandler => {
    return new HandlerExpress(logger, _express, args, middlewares, controllers)
  }

  /**
   * Method to get instance of middleware Body Parser JSON
   *
   * @return IMiddleware
   */
  public static getMiddlewareBodyParserJSON = (): IMiddleware => {
    return new BodyParserJSONMiddleware()
  }

  /**
   * Method to get instance of middleware Body Parser Url Encoded
   *
   * @return IMiddleware
   */
  public static getMiddlewareBodyParserUrlEncoded = (): IMiddleware => {
    return new BodyParserUrlEncodedMiddleware()
  }

  /**
   * Method to get instance of middleware Logger
   *
   * @return IMiddleware
   */
  public static getMiddlewareLogger = (logger: ILogger): IMiddleware => {
    return new LoggerMiddleware(logger)
  }
}
