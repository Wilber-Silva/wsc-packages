/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import {
  IDatabaseRepository,
  ILogger,
  IServiceCrud,
  _id,
} from '@wsc-company/types'

export class BaseCrudService implements IServiceCrud {
  constructor(
    /**
     * Logger
     */
    protected readonly _logger: ILogger,
    /**
     * Default Repository
     */
    readonly _repository: IDatabaseRepository,
  ) {}

  get<T>(query: any): Promise<T[]> {
    return this.find(query)
  }
  list<T>(query: any): Promise<T[]> {
    return this.find(query)
  }
  find<T>(query: any): Promise<T[]> {
    this._logger.debug('Find', { query })
    throw new Error('Method not implemented.')
  }
  findById<T>(id: string): Promise<T> {
    return this._repository.findById<T>(id)
  }
  findOne<T>(query: any): Promise<T> {
    this._logger.debug('Find One', { query })
    throw new Error('Method not implemented.')
  }
  getOne<T>(query: any): Promise<T> {
    return this.findOne(query)
  }
  create<T>(data: T): Promise<T> {
    return this._repository.create<T>(data)
  }
  createMany<T>(collectionData: T[]): Promise<T[]> {
    return this._repository.createMany<T>(collectionData)
  }
  update<T>(id: string, data: T): Promise<T> {
    return this._repository.update<T>(id, data)
  }
  updateMany<T>(collectionData: T[]): Promise<boolean> {
    return this._repository.updateMany<T>(collectionData)
  }
  delete(id: _id): Promise<boolean> {
    return this._repository.delete(id)
  }
  deleteMany(ids: _id[]): Promise<boolean> {
    return this._repository.deleteMany(ids)
  }
}
