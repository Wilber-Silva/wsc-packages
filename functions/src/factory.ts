import {
  IBaseControllerFunction,
  IHandlerFunctions,
  ILogger,
  IMiddleware,
} from '@wsc/types'
import { HandlerFunctions } from './handlers'

export class Factory {
  /**
   * Method to get handler of Google Functions
   *
   * @param logger Logger to log application
   * @param controllers Controllers to Google Functions
   * @return IHandlerFunctions
   */
  public static getHandler = (
    logger: ILogger,
    controllers: { [index: string]: IBaseControllerFunction },
  ): IHandlerFunctions => {
    return new HandlerFunctions(logger, controllers)
  }
}
