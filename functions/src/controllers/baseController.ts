import {
  FirestoreDocumentCreateListen,
  PubSubMessageListenEvent,
  PubSubScheduleEvent,
  HttpsFunction,
  IonWrite,
  IonCreate,
} from '@wsc/firebase'
import { IBaseControllerFunction, ILogger } from '@wsc/types'
import * as functions from 'firebase-functions'
// import * as admin from 'firebase-admin'

export abstract class BaseController implements IBaseControllerFunction {
  /**
   * Ignore it for now ...
   */
  public path: string = null
  public router: any = null

  /**
   * Method to construct instance of Controller
   *
   * @param _logger Logger to log application
   * @param label Label of this controller
   * @param _service Service of this controller
   * @param _repository Repository of this controller
   */
  constructor(
    /**
     * Logger to log application
     * @Param ILogger
     */
    protected _logger: ILogger,

    /**
     * Label of this controller
     * @param string
     */
    public label: string,

    /**
     * Service to execute in datasource
     * @param any
     */
    private _service: any,

    /**
     * Repository to load data
     * @param any
     */
    private _repository: any,
  ) {}

  /**
   * Method to return a event to Google Function
   *
   * @return any
   */
  public event = (): any => null

  /**
   * Method to get callback to PubSub Message Listen
   *
   * @param topic Topic to listen
   * @param event Callback to code event
   * @return any
   */
  protected getPubSubMessageListen = (
    topic: string,
    event: (
      message: functions.pubsub.Message,
      context: functions.EventContext,
    ) => any,
  ): any => {
    this._logger.info('Try listen pubsub message')
    try {
      const responseEvent = PubSubMessageListenEvent.getEvent(
        this._logger,
        topic,
        event,
      )
      this._logger.info('Success listen messages pubsub')
      return responseEvent
    } catch (err) {
      this._logger.exception(err)
      throw err
    } finally {
      this._logger.info('Finish try listen pubsub')
    }
  }

  /**
   * Method to get callback to https function
   *
   * @param event Callback to execute on event htts function
   * @return any
   */
  protected getHttpsFunction = (
    event: (
      request: functions.https.Request,
      response: functions.Response,
    ) => any,
  ): any => {
    this._logger.info('Try https function')
    try {
      const responseEvent = HttpsFunction.getEvent(this._logger, event)
      this._logger.info('Success https function')
      return responseEvent
    } catch (err) {
      this._logger.exception(err)
      throw err
    } finally {
      this._logger.info('Finish try https function')
    }
  }

  /**
   * Method to get callback to PubSub Scheduler
   *
   * @param rule RUle time to apply in schedule, eg: every monday 09:00
   * @param event Callback to code event
   * @param timezone Timezone of rule time
   * @return any
   */
  protected getPubSubScheduler = (
    rule: string,
    event: (context: functions.EventContext) => any,
    region = 'us-east1',
    timezone = 'America/Sao_Paulo',
  ): any => {
    return PubSubScheduleEvent.getEvent(
      this._logger,
      rule,
      event,
      timezone,
      region,
    )
  }

  /**
   * Method to get callback to Firestore Document Listen
   *
   * @param document Document to listen
   * @param event Callback to code event
   * @return any
   */
  protected getFirestoreDocumentListen = (
    params: {
      document: string
      onWrite?: boolean
      onCreate?: boolean
      onUpdate?: boolean
    },
    event: (params: IonWrite | IonCreate) => any,
  ): any => {
    return FirestoreDocumentCreateListen.getEvent(this._logger, event, params)
  }

  /**
   * Ignore it for now ...
   */
  public initRoutes = (): void => null
  public setSchema = (schema: any, factorySchema: (query: any) => any): void =>
    null
  public validationSchema = (): any => null
  public requestValidate = async (
    validationSchema: Record<string, any>,
    input: Record<string, any>,
  ): Promise<any> => null
  public create = async (): Promise<any> => null
  public createMany = async (): Promise<string[]> => null
  public find = async <T>(): Promise<T> => null
  public update = async (): Promise<void> => null
  public delete = async (): Promise<void> => null
}
