import {
  IApplication,
  IBaseControllerFunction,
  IHandlerFunctions,
  ILogger,
  IMiddleware,
} from '@wsc/types'

export class HandlerFunctions implements IHandlerFunctions {
  /**
   * Method to construct instance of handler to Google Functions
   *
   * @param _logger
   * @param _functions
   * @param _args
   * @param _middlewares
   * @param _controllers
   */
  constructor(
    private _logger: ILogger,
    private _controllers: { [index: string]: IBaseControllerFunction },
  ) {}

  /**
   * Method to get controller for Google Function
   *
   * @param index Index of controller on construct
   * @return any
   */
  public getController = (index: string): any => {
    this._logger.info('get Controller to Google Function', {
      name: index,
    })
    try {
      if (this._controllers[index]) return this._controllers[index].event()
    } catch (err) {
      this._logger.exception(err)
      throw err
    }
    this._logger.error('Controller for Google Function NOT FOUND', {
      name: index,
    })
    return null
  }

  /**
   * Ignore it for now ...
   */
  public dispach = (application: IApplication): void => null
  public getInstance = (): any => null
  public stop = (): void => null
  public getArgs = (): { [index: string]: string | number } => null
  private middlewares = (): void => null
  private functions = (): void => null
  public getMiddlewaresDefaut = (): IMiddleware[] => null
}
