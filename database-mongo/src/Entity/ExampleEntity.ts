import { IExampleSchema } from '@wsc-company/types'
import { BaseMongoDbEntity as BaseEntity } from './baseMongoDbEntity'
import { prop } from '@hasezoey/typegoose'

export class ExampleEntity extends BaseEntity implements IExampleSchema {
  @prop({ required: true })
  name: string
}
