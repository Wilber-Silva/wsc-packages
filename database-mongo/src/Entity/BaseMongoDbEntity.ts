import { prop, Typegoose } from '@hasezoey/typegoose'
import { IBaseSchema, date, _id } from '@wsc-company/types'
import { v4 as generateId } from 'uuid'

export class BaseMongoDbEntity extends Typegoose implements IBaseSchema {
  @prop({ required: true, default: () => generateId() })
  _id?: _id
  @prop({ required: true, default: () => new Date() })
  createdAt?: date
  @prop({ default: null })
  updatedAt?: date
  @prop({ default: null })
  deletedAt?: date
}
