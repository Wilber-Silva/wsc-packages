import { ExampleEntity } from '.'

export class FactoryMongoDbEntity {
  static getExampleEntity(): ExampleEntity {
    return new ExampleEntity()
  }
}
