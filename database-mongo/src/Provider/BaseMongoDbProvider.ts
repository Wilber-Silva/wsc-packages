import { IOptions } from './../IOptions'
import { BaseMongoDbEntity as BaseEntity } from './../entity'
import {
  IConnection,
  IDataBaseProvider,
  _id,
  IBaseSchema,
} from '@wsc-company/types'

export abstract class BaseMongoDbProvider implements IDataBaseProvider {
  /**
   * Mongoose Entity Model class extends all method of mongoose
   */
  readonly entity
  constructor(
    /**
     * Connection
     */
    protected readonly connection: IConnection,
    /**
     * Data Base entity
     */
    entity: BaseEntity,
  ) {
    this.entity = entity.getModelForClass(entity)
  }
  /**
   * Create connection
   */
  async createConnection(): Promise<void> {
    await this.connection.connect()
  }
  /**
   * Close Connection
   */
  async closeConnection(): Promise<void> {
    await this.connection.close()
  }
  /**
   * Find Collection data into data base
   * @param options Options to find a data
   */
  find<T extends IBaseSchema>(options?: IOptions<T>): Promise<T[]> {
    if (!options) options = {}
    return this.entity
      .find(options.query)
      .limit(options.take)
      .skip(options.skip)
      .sort(options.sortBy)
      .select(options.select)
  }
  /**
   * List Collection data into data base
   * @param options Options to find a data
   */
  list<T extends IBaseSchema>(options?: IOptions<T>): Promise<T[]> {
    return this.find<T>(options)
  }
  /**
   * Find one item on data base collection
   * @param options Options to find a data
   */
  findOne<T extends IBaseSchema>(options: IOptions<T>): Promise<T> {
    return this.entity.findOne(options.query).select(options.select)
  }
  /**
   * Find one item on data base collection or create this
   * @param options Options to try to find a data
   * @param data Data to create if not found
   */
  async findOrCreate<T extends IBaseSchema>(
    options: IOptions<T>,
    data?: T,
  ): Promise<T> {
    let founded = await this.findOne<T>(options)
    if (!founded && data) founded = await this.create(data)

    return founded
  }
  /**
   * Find one item on data base collection by id
   * @param id Id to find
   */
  findById<T extends IBaseSchema>(id: _id): Promise<T> {
    return this.entity.findById(id)
  }
  /**
   * Create data on data base collection
   * @param data Data to create on collection
   */
  create<T extends IBaseSchema>(data: T): Promise<T> {
    return this.entity.create(data)
  }
  /**
   * Create many items on data base collection
   * @param collectionToCreate Collection of data to create
   */
  async createMany<T extends IBaseSchema>(
    collectionToCreate: T[],
  ): Promise<T[]> {
    return this.entity.insertMany(collectionToCreate)
  }
  /**
   * Update data base collection item
   * @param id Id of item
   * @param data Data to update
   */
  update<T extends IBaseSchema>(id: _id, data: T): Promise<T> {
    if (data._id) delete data._id
    data.updatedAt = new Date()
    return this.entity.findByIdAndUpdate(id, data)
  }
  /**
   * Update many items on data base
   * @param options Not use on mongodb
   * @param collection Collection to update
   */
  async updateMany<T extends IBaseSchema>(
    collection: T[],
    options?: IOptions<T>,
  ): Promise<boolean> {
    const updatePromises = collection.map((item) => this.update(item._id, item))
    return Promise.all(updatePromises)
      .then(() => true)
      .catch(() => false)
  }
  /**
   * Delete a item of data base
   * @param id Id of item to delete
   * @param force able or disable soft deletes
   */
  async delete(id: _id, force = false): Promise<boolean> {
    if (!force) {
      const updated = await this.update(id, { deletedAt: new Date() })
      return !!updated
    }
    const deleted = await this.entity.deleteOne({ _id: id })
    return !!deleted
  }
  /**
   * Delete many items of data base
   * @param ids Collection os ids to delete
   * @param force able or disable soft deletes
   */
  async deleteMany(ids: _id[], force = false): Promise<boolean> {
    if (!force) {
      const updated = await this.entity.updateMany(
        {
          query: {
            _id: { $in: ids },
          },
        },
        { $set: { updatedAt: new Date(), deletedAt: new Date() } },
      )
      return !!updated
    }

    const deleted = await this.entity.deleteMany({ _id: { $in: ids } })
    return !!deleted
  }
}
