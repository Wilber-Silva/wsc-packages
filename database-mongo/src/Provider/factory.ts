import { MongoConnection } from './../mongoDbConnection'
import { FactoryMongoDbEntity } from './../entity'
import { ExampleProvider } from '.'

const { DEFAULT_MONGODB } = process.env

export class FactoryMongoDbProvider {
  static getExampleProvider(db: string = DEFAULT_MONGODB): ExampleProvider {
    return new ExampleProvider(
      new MongoConnection(db),
      FactoryMongoDbEntity.getExampleEntity(),
    )
  }
}
