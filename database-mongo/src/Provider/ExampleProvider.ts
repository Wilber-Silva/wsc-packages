import { IDataBaseProvider } from '@wsc-company/types'
import { BaseMongoDbProvider } from './baseMongoDbProvider'

export class ExampleProvider
  extends BaseMongoDbProvider
  implements IDataBaseProvider {}
