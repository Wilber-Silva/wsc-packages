import { IExampleSchema } from '@wsc-company/types'
import { IBaseSchema } from '@wsc-company/types/'
import { FilterQuery } from 'mongoose'

export interface IOptions<Q extends IBaseSchema> {
  query?: FilterQuery<Q>
  take?: number
  skip?: number
  sortBy?: string | any
  select?: string | any
}
