import { IConnection } from '@wsc-company/types'
import * as mongoose from 'mongoose'

export class MongoConnection implements IConnection {
  /**
   * Connection instance
   */
  connection: mongoose.Mongoose

  constructor(
    /**
     * Mongo Data Base connection url
     */
    private readonly url: string,
    /**
     * Mongoose connection options
     */
    private options?: mongoose.ConnectionOptions,
    private unique = true,
  ) {
    if (!this.options)
      this.options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }
  }

  /**
   * Create connection
   */
  async connect(): Promise<void> {
    if (this.unique) {
      this.connection = await mongoose.connect(this.url, this.options)
    }
  }

  /**
   * Close connection
   */
  async close(): Promise<void> {
    if (this.unique) {
      await mongoose.connection.close()
    }
  }
}
