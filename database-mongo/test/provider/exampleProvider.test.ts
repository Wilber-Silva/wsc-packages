import { IExampleSchema } from '@wsc-company/types'
import { FactoryMongoDbProvider, ExampleProvider } from './../../src'
describe('Test Example Provider', () => {
  let provider: ExampleProvider = null

  // to single test
  const name = 'test-robot'
  let id: string = ''

  // to many tests
  const examples: IExampleSchema[] = [
    { name },
    { name: `${name}-1` },
    { name: `${name}-2` },
    { name: `${name}-3` },
    { name: `${name}-4` },
  ]

  let ids: string[] = []

  beforeAll(async () => {
    provider = FactoryMongoDbProvider.getExampleProvider()
    await provider.createConnection()
  })

  it('Has been defined', () => {
    expect(provider).toBeDefined()
  })

  it('Find All', async () => {
    const fondedAll = await provider.find()
    const list = await provider.list()

    expect(fondedAll).toBeDefined()
    expect(list).toBeDefined()
  })

  it('Create', async () => {
    const create = await provider.create<IExampleSchema>({ name })

    expect(create.name).toBe(name)

    id = create._id
  })

  it('Find by id', async () => {
    const findById = await provider.findById<IExampleSchema>(id)

    expect(findById.name).toBe(name)
  })

  it('Find by name', async () => {
    const fonded = await provider.findOne<IExampleSchema>({ query: { name } })

    expect(fonded.name).toBe(name)
  })

  it('Update by name', async () => {
    const update = await provider.update<IExampleSchema>(id, { name })

    expect(update.name).toBe(name)
  })

  it('Delete', async () => {
    const deleted = await provider.delete(id, true)

    expect(deleted).toBe(true)
  })

  it('Find Or Create', async () => {
    const findOrCreate = await provider.findOrCreate<IExampleSchema>(
      { query: { name } },
      { name },
    )

    expect(findOrCreate.name).toBe(name)

    await provider.delete(findOrCreate._id)
  })

  it('Create Many', async () => {
    const createMany = await provider.createMany<IExampleSchema>(examples)

    expect(createMany.length).toBe(examples.length)

    ids = createMany.map((item) => item._id)
  })

  it('Update Many', async () => {
    const updateMany = await provider.updateMany<IExampleSchema>([
      { _id: id, name },
    ])

    expect(updateMany).toBe(true)
  })

  it('Delete Many', async () => {
    const deleteMany = await provider.deleteMany(ids)

    expect(deleteMany).toBe(true)
  })

  it('Delete Many Force', async () => {
    const deletedManyForce = await provider.deleteMany(ids, true)

    expect(deletedManyForce).toBe(true)
  })

  it('Close', async () => {
    await provider.closeConnection()
  })
})
