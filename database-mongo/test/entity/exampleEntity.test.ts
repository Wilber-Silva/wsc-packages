import { FactoryMongoDbEntity, ExampleEntity } from './../../src'

describe('Test Example Entity', () => {
  let entity: ExampleEntity = null

  beforeEach(() => {
    entity = FactoryMongoDbEntity.getExampleEntity()
  })

  it('Has been defined', () => {
    expect(entity).toBeDefined()
  })
})
