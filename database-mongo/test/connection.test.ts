import { MongoConnection } from './../src'

describe('Connection to MongoDb', () => {
  const testDbEndpoint = process.env.DEFAULT_MONGODB
  let connector: MongoConnection = null

  beforeEach(() => {
    connector = new MongoConnection(testDbEndpoint)
  })

  it('Has been defined', () => {
    expect(connector).toBeDefined()
  })

  it('Create connection', () => {
    connector.connect()
  })

  it('Close connection', () => {
    connector.close()
  })
})
