# Database MongoDb #

Pacote de Com conexão e modelos de regras para o mongodb

## Entity and Provider ##

* Entity

As Entities básicamente são os modelos de dados, com os campos, preenchimento **default**, qual campo é obrigatório ou unico, etc.

* Provider

Os providers são os detentores das regras de db como fazer uma query? O provider quem sabe, como pegar um user pelo e-mail ou pelo cpf? o provider que sabe.

Em outras palavras o provider é a ponte entre o restante do código e a Entity.

## Usages ##

* new entity example

Sempre que for criar uma nova entity siga a regra de extender o **BaseMongoDbEntity** e implementar o schema do mesmo contexto como no exemplo o schema correto é **IExampleSchema**

Então crie um novo metodo na factory para instanciar a nova entity

~~~javascript
import { IExampleSchema } from '@wsc-company/types'
import { BaseMongoDbEntity as BaseEntity } from './baseMongoDbEntity'
import { prop } from '@hasezoey/typegoose'

export class ExampleEntity extends BaseEntity implements IExampleSchema {
  @prop({ required: true })
  name: string
}

~~~

* new provider example

Sempre que for criar um novo provider siga a regra de extender o **BaseMongoDbProvider** e implementar o **IDataBaseProvider**.

Então crie um metodo na factory para instanciar esse provider

~~~javascript
import { IDataBaseProvider } from '@wsc-company/types'
import { BaseMongoDbProvider } from './baseMongoDbProvider'

export class ExampleProvider
  extends BaseMongoDbProvider
  implements IDataBaseProvider {}

export class FactoryMongoDbProvider {
  static getExampleProvider(db: string = DEFAULT_MONGODB): ExampleProvider {
    return new ExampleProvider(
      new MongoConnection(db),
      FactoryMongoDbEntity.getExampleEntity(),
    )
  }
}

~~~

* BaseProvider methods

Os metodos dentro do BaseProvider foram inspirados nos metodos do próprio mongo então a forma de usar é bem parecida, para maiores informações sobre como usa-los procure em /test dentro do próprio projeto
