import { generateId } from './../../../Helpers/index'
import { IFirestoreFindWhere } from './IFirestoreFindWhere'
import { IEntity } from './../../IEntity'
import { BaseFireStoreEntity as BaseEntity } from './../Entity/BaseEntity'
import { FireStoreConnection } from './../FireStoreConnection'
import {
  CollectionReference,
  DocumentData,
  Firestore,
  QueryDocumentSnapshot,
  QuerySnapshot,
  WriteBatch,
  WriteResult,
} from '@google-cloud/firestore'
import { IDataBaseProvider, IOptions } from './../../IDataBaseProvider'
import { IFirestoreOptionsQuery } from './IFirestoreOptionsQuery'

export class BaseFireStoreProvider implements IDataBaseProvider {
  protected firestore: Firestore
  protected batch: WriteBatch
  protected collection: CollectionReference
  protected entity: BaseEntity
  protected dateNow: Date = new Date()
  protected readonly _defaultWhereAnd: IFirestoreFindWhere[] = [
    {
      field: 'deletedAt',
      op: '==',
      value: null,
    },
  ]
  constructor(
    /**
     * Firestore connection instance
     */
    protected readonly connection: FireStoreConnection,
    protected readonly _entityFactory: <T extends IEntity>(
      fields: IEntity,
    ) => T,
    /**
     * Data base collection name
     */
    protected readonly collectionName: string = 'entity',
  ) {}
  protected generateId() {
    return generateId()
  }
  protected async _factoryCollection() {
    return this.firestore.collection(this.collectionName)
  }
  protected async _find(
    options: IFirestoreOptionsQuery,
  ): Promise<QuerySnapshot<DocumentData>> {
    const collection = await this._factoryCollection()
    const whereAnd = [...(options.whereAnd || []), ...this._defaultWhereAnd]
    if (whereAnd) {
      whereAnd.map((where) => {
        collection.where(where.field, where.op, where.value)
      })
    }
    if (options.limit) {
      collection.limit(options.limit)
    }
    if (options.orderBy) {
      collection.orderBy(options.orderBy)
    }
    if (options.select) {
      collection.select(options.select)
    }
    return collection.get()
  }
  protected _convertToObject<T>(
    snapshot: QueryDocumentSnapshot | DocumentData,
  ): T {
    return this._entityFactory({
      _id: snapshot.id,
      ...snapshot.data(),
    })
  }
  protected _convertToCollection<T extends IEntity>(
    cursor: QuerySnapshot | Record<string, any>,
  ): T[] {
    const collection = []
    if (!cursor || cursor.empty) {
      return collection
    }
    cursor.forEach((element: QueryDocumentSnapshot) => {
      collection.push(this._convertToObject(element))
    })
    return collection
  }
  protected toFirestore<T extends IEntity>(object: T): IEntity {
    if (object._id) delete object._id
    return { ...object }
  }
  async createConnection(): Promise<void> {
    await this.connection.connect()
    this.firestore = this.connection.firestore
    this.batch = this.firestore.batch()
  }
  async closeConnection(): Promise<void> {
    await this.connection.close()
  }
  async find<T extends IEntity>(options: IFirestoreOptionsQuery): Promise<T[]> {
    const querySnapshot = await this._find(options)
    return this._convertToCollection<T>(querySnapshot)
  }
  list<T extends IEntity>(options: IFirestoreOptionsQuery): Promise<T[]> {
    return this.find(options)
  }
  async findOne<T extends IEntity>(options: IOptions): Promise<T> {
    const querySnapshot = await this.find({
      ...options,
      limit: 1,
    })

    const collection = this._convertToCollection<T>(querySnapshot)

    return collection[0] || null
  }
  async findOrCreate<T extends IEntity>(data: T): Promise<T> {
    throw new Error('Must to implement on your class')
  }
  async findById<T extends IEntity>(_id: string): Promise<T> {
    const collection = await this._factoryCollection()
    const document = await this._convertToObject(collection.doc(_id))
    return this._convertToObject(document)
  }
  async create<T extends IEntity>(data: T, _id?: string): Promise<T> {
    data.createdAt = this.dateNow.toISOString()
    data.updatedAt = null
    data.deletedAt = null

    if (!_id) _id = this.generateId()
    const collection = await this._factoryCollection()
    const firestoreData = await this.toFirestore(data)
    const created: WriteResult = await collection
      .doc(_id)
      .set(firestoreData, { merge: true })

    if (!created) {
      throw new Error('Created Fail')
    }

    return this._entityFactory<T>({
      _id,
      ...data,
    })
  }
  async createMany<T extends IEntity>(collectionToCreate: T[]): Promise<T[]> {
    const collection = await this._factoryCollection()
    const writeResult: T[] = []
    for (const item of collectionToCreate) {
      let _id = item._id
      if (!_id) _id = this.generateId()
      const firestoreData = await this.toFirestore(item)
      const reference = collection.doc(_id)

      this.batch.set(reference, firestoreData)

      writeResult.push(
        this._entityFactory<T>({
          _id,
          ...item,
        }),
      )
    }

    const created = await this.batch.commit()

    if (created.length !== writeResult.length) {
      throw new Error(
        `Original collection to create and created on batch is difference result. original: ${writeResult.length} created: ${created.length}`,
      )
    }

    return writeResult
  }
  async update<T extends IEntity>(_id: string, data: T): Promise<T> {
    data.updatedAt = new Date().toISOString()

    const collection = await this._factoryCollection()
    const firestoreData = this.toFirestore(data)
    const updated: WriteResult = await collection.doc(_id).update(firestoreData)

    if (!updated) {
      throw new Error('Update fail')
    }

    return this._entityFactory<T>({
      _id,
      ...data,
    })
  }
  async updateMany<T extends IEntity>(
    collectionToUpdate: T[],
  ): Promise<boolean> {
    const collection = await this._factoryCollection()
    const writeResult: T[] = []
    for (const item of collectionToUpdate) {
      let _id = item._id
      if (!_id) {
        _id = this.generateId()
      }
      item.updatedAt = new Date().toISOString()
      const firestoreData = await this.toFirestore(item)
      const reference = collection.doc(_id)

      this.batch.update(reference, firestoreData)

      writeResult.push(
        this._entityFactory<T>({
          _id,
          ...item,
        }),
      )
    }

    const updated = await this.batch.commit()

    if (updated.length !== writeResult.length) {
      throw new Error(
        `Original collection to update and updated on batch is difference result. original: ${writeResult.length} updated: ${updated.length}`,
      )
    }

    return true
  }
  async delete(_id: string, force: boolean = false): Promise<boolean> {
    if (force) {
      const collection = await this._factoryCollection()
      const deleted: WriteResult = await collection.doc(_id).delete()

      if (!deleted) {
        throw new Error('Delete Fail')
      }
    } else {
      await this.update(_id, {
        deletedAt: new Date().toDateString(),
      })
    }

    return true
  }
  async deleteMany(ids: string[], force: boolean = false): Promise<boolean> {
    const collection = await this._factoryCollection()
    const deleteResult: string[] = []
    if (force) {
      for (const _id of ids) {
        const reference = collection.doc(_id)

        this.batch.delete(reference)

        deleteResult.push(_id)
      }
    } else {
      for (const _id of ids) {
        const reference = collection.doc(_id)

        this.batch.update(reference, {
          updatedAt: new Date().toISOString(),
        })

        deleteResult.push(_id)
      }
    }

    const deleted = await this.batch.commit()

    if (deleted.length !== deleteResult.length) {
      throw new Error(
        `Original collection to delete and deleted on batch is difference result. original: ${deleteResult.length} deleted: ${deleted.length}`,
      )
    }

    return true
  }
}
