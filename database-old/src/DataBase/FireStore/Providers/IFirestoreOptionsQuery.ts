import { FieldPath } from '@google-cloud/firestore'
import { IFirestoreFindWhere } from './IFirestoreFindWhere'
import { IFireStoreQueryOptions as IQueryOptions } from './IQueryOptions'
export interface IFirestoreOptionsQuery extends IQueryOptions {
  /**
   * Targets fields to get
   */
  select?: string | FieldPath
  /**
   * Add where in query to Firestore
   * @param IFirestoreFindWhere
   */
  whereAnd?: IFirestoreFindWhere[]
  /**
   * Limit to get documents
   * @param number
   */
  limit?: number
  /**
   * Order of documents collection
   * @param orderBy
   */
  orderBy?: string | FieldPath
  /**
   * Able to list trashed
   * @param trashed
   */
  trashed?: boolean
}
