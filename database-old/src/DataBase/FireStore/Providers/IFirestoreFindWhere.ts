import { WhereFilterOp, Timestamp } from '@google-cloud/firestore'
export interface IFirestoreFindWhere {
  /**
   * Field to use in query
   * @param string
   */
  field: string
  /**
   * Operation to use in query
   * @param WhereFilterOp
   */
  op: WhereFilterOp
  /**
   * Value to use in query
   * @param string
   */
  value: string | number | Timestamp | boolean
}
