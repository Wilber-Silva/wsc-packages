import { IEntity } from './../../IEntity'
import { Base, property } from 'pring'

export class BaseFireStoreEntity extends Base implements IEntity {
  @property
  _id?: string
  @property
  createdAt: Date
  @property
  updatedAt: Date
  @property
  deletedAt: Date
}
