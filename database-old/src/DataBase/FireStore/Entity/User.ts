import {
  Gender,
  IUserAddressSchema,
  IUserDocumentSchema,
  IUserPhoneSchema,
  IUserSchema,
  DocumentType,
} from 'src/DataBase/Schemas'
import { BaseFireStoreEntity } from './BaseEntity'
import { Base, property } from 'pring'

class UserAddress extends Base implements IUserAddressSchema {
  @property
  city?: string
  @property
  country?: string
  @property
  zip?: string
  @property
  district?: string
}

class UserPhone extends Base implements IUserPhoneSchema {
  @property
  number?: number
  @property
  name?: string
}

class UserDocument extends Base implements IUserDocumentSchema {
  @property
  number?: string
  @property
  type?: DocumentType
}

export class UserFireStoreEntity extends BaseFireStoreEntity
  implements IUserSchema {
  @property
  fullName?: string
  @property
  firstName?: string
  @property
  lastName?: string
  @property
  login?: string
  @property
  birthDate?: Date
  @property
  gender?: Gender
  @property
  address?: UserAddress[]
  @property
  phones?: UserPhone[]
  @property
  document?: UserDocument[]
  @property
  isBlocked: boolean
}
