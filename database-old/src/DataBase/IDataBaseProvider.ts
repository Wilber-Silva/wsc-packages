import { IEntity } from './IEntity'

export type id = string | number

export interface IOptions {
  query?: any
  skip?: number
  take?: number
  select?: any
  sortBy?: any
}

export interface IDataBaseProvider {
  /**
   * Create data base connection
   */
  createConnection(): Promise<void>
  /**
   * Destroy data base connection
   */
  closeConnection(): Promise<void>
  /**
   * Options to get a collection
   * @param options
   * @returns Collections off data
   */
  find<T extends IEntity>(options: any): Promise<T[]>
  /**
   * Options to get a collection
   * @param options
   * @returns Collections off data
   */
  list<T extends IEntity>(options: any): Promise<T[]>
  /**
   * Options to get data
   * @param options
   * @returns Only one line of data
   */
  findOne<T extends IEntity>(options: any): Promise<T>
  /**
   * Data object find one or create this if not exists
   * @param data
   * @returns Data
   */
  findOrCreate<T extends IEntity>(data: T): Promise<T>
  /**
   * Id of line
   * @param _id
   * @returns One data of line
   */
  findById<T extends IEntity>(_id: id): Promise<T>
  /**
   * Data to create
   * @param data
   * @returns Data with id
   */
  create<T extends IEntity>(data: T, _id?: id): Promise<T>
  /**
   * Collection to create
   * @param collectionToCreate
   * @returns All data with _id
   */
  createMany<T extends IEntity>(collectionToCreate: T[]): Promise<T[]>
  /**
   * Id of line
   * @param _id
   * Data to update
   * @param data
   * @returns Data updated
   */
  update<T extends IEntity>(_id: id, data: T): Promise<T>
  /**
   * Collection to update
   * @param collectionToUpdate
   * @returns Collection of data with a flag
   */
  updateMany<T extends IEntity>(data: T[]): Promise<boolean>

  /**
   * Id of data line
   * @param _id
   * Force to delete forever or continue with a logic deleted
   * @param force
   * @returns True if is success deleted
   */
  delete(_id: id, force?: boolean): Promise<boolean>
  /**
   * Ids of lines to delete
   * @param ids
   * Force to forever delete
   * @param force
   * @returns Collection data with flag
   */
  deleteMany(ids: id[], force?: boolean): Promise<boolean>
}
