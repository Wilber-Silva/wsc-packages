import { prop } from '@hasezoey/typegoose'
import { IUserSchema, Gender } from './../../Schemas/IUserSchema'
import {
  IUserDocumentSchema,
  DocumentType,
} from './../../Schemas/IUserDocumentSchema'
import { IUserPhoneSchema } from './../../Schemas/IUserPhoneSchema'
import { IUserAddressSchema } from './../../Schemas/IUserAddressSchema'
import { BaseMongoEntity as BaseEntity } from './BaseEntity'

class AddressSchema extends BaseEntity implements IUserAddressSchema {
  @prop()
  city?: string
  @prop()
  country?: string
  @prop()
  zip?: string
  @prop()
  district?: string
}

class PhoneSchema extends BaseEntity implements IUserPhoneSchema {
  @prop()
  number: number
  @prop()
  name: string
}

class UserDocumentSchema extends BaseEntity implements IUserDocumentSchema {
  @prop()
  number: string
  @prop()
  type: DocumentType
}

export class UserMongoEntity extends BaseEntity implements IUserSchema {
  @prop()
  _id: string
  @prop()
  fullName: string
  @prop()
  firstName?: string
  @prop()
  lastName?: string
  @prop()
  login: string
  @prop()
  birthDate: Date
  @prop()
  gender: Gender
  @prop()
  address?: AddressSchema[]
  @prop()
  phones?: PhoneSchema[]
  @prop()
  document: UserDocumentSchema[]
  @prop()
  isBlocked: boolean = false
  @prop()
  createdAt: Date = new Date()
  @prop()
  updatedAt: Date = null
  @prop()
  deletedAt: Date = null
}
