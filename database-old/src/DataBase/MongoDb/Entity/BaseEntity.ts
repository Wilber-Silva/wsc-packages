import { IEntity } from './../../IEntity'
import { prop, Typegoose } from '@hasezoey/typegoose'

export class BaseMongoEntity extends Typegoose implements IEntity {
  @prop()
  id?: string
  @prop()
  createdAt: Date = new Date()
  @prop()
  updatedAt: Date = null
  @prop()
  deletedAt: Date = null
}
