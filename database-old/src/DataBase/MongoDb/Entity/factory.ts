import { UserMongoEntity } from './User'

export class FactoryMongoEntity {
  static getUserEntity(): UserMongoEntity {
    return new UserMongoEntity()
  }
}
