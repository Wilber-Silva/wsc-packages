import { MongoConnection } from './../MongoDbConnection'
import { FactoryMongoEntity } from './../Entity/factory'
import { UserMongoProvider } from './UserProvider'

const DB =
  'mongodb+srv://wcosta:413105839ww@cluster0-om5wx.mongodb.net/user?retryWrites=true&w=majority'

export class FactoryMongoProvider {
  static getUserProvider(): UserMongoProvider {
    return new UserMongoProvider(
      FactoryMongoEntity.getUserEntity(),
      new MongoConnection(DB),
    )
  }
}
