import { IDataBaseProvider } from './../../IDataBaseProvider'
import { BaseProvider } from './BaseProvider'

export class UserMongoProvider extends BaseProvider
  implements IDataBaseProvider {}
