import { generateId } from './../../../Helpers'
import { IConnection } from './../../IConnection'
import { IEntity } from './../../IEntity'
import { id, IDataBaseProvider, IOptions } from './../../IDataBaseProvider'
import { BaseMongoEntity as BaseEntity } from '../Entity/BaseEntity'

export abstract class BaseProvider implements IDataBaseProvider {
  /**
   * Mongoose Entity Model class extends all method of mongoose
   */
  private readonly entity
  constructor(
    /**
     * Data Base entity
     */
    entity: BaseEntity,
    /**
     * Connection
     */
    protected readonly connection: IConnection,
  ) {
    this.entity = entity.getModelForClass(entity)
  }
  async createConnection(): Promise<void> {
    await this.connection.connect()
  }
  async closeConnection(): Promise<void> {
    await this.connection.close()
  }
  protected generateId() {
    return generateId()
  }
  find<T extends IEntity>(options?: IOptions): Promise<T[]> {
    if (!options) options = {}
    if (!options.query) options.query = {}
    return this.entity
      .find(options.query)
      .limit(options.take)
      .skip(options.skip)
      .sort(options.sortBy)
      .select(options.select)
  }
  list<T extends IEntity>(options: IOptions): Promise<T[]> {
    return this.find<T>(options)
  }
  findOne<T extends IEntity>(options: IOptions): Promise<T> {
    return this.entity.findOne(options.query).select(options.select)
  }
  async findOrCreate<T extends IEntity>(data: T): Promise<T> {
    let founded = await this.findOne<T>({
      query: data,
    })
    if (!founded) founded = await this.create(data)

    return founded
  }
  findById<T extends IEntity>(_id: id): Promise<T> {
    return this.entity.findById(_id)
  }
  create<T extends IEntity>(data: T): Promise<T> {
    if (!data._id) data._id = this.generateId()
    return this.entity.create(data)
  }
  async createMany<T extends IEntity>(collectionToCreate: T[]): Promise<T[]> {
    collectionToCreate = await collectionToCreate.map((item) => ({
      ...item,
      _id: !item._id ? this.generateId : item._id,
    }))
    return this.entity.insertMany(collectionToCreate)
  }
  update<T extends IEntity>(_id: id, data: T): Promise<T> {
    if (data._id) delete data._id
    data.updatedAt = new Date()
    return this.entity.findByIdAndUpdate(_id, data)
  }
  async updateMany<T extends IEntity>(
    options: IOptions,
    collection: T[],
  ): Promise<boolean> {
    const updatePromises = collection.map((item) => this.update(item._id, item))
    return Promise.all(updatePromises)
      .then(() => true)
      .catch(() => false)
  }
  async delete(_id: id, force = false): Promise<boolean> {
    if (!force) {
      const updated = await this.update(_id, {})
      return !!updated
    }
    const deleted = await this.entity.deleteOne({ _id })
    return !!deleted
  }
  async deleteMany(ids: id[], force = false): Promise<boolean> {
    if (!force) {
      const updated = await this.entity.updateMany(
        {
          query: {
            $in: { _id: ids },
          },
        },
        { $set: { updatedAt: new Date(), deletedAt: new Date() } },
      )
      return !!updated
    }

    const deleted = await this.entity.delete({ $in: ids })
    return !!deleted
  }
}
