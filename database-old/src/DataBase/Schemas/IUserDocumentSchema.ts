export enum DocumentType {
  cpf = 'cpf',
}

export interface IUserDocumentSchema {
  number?: string
  type?: DocumentType
}
