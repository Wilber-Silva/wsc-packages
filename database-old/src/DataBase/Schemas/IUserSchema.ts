import { IUserDocumentSchema } from './IUserDocumentSchema'
import { IUserPhoneSchema } from './IUserPhoneSchema'
import { IUserAddressSchema } from './IUserAddressSchema'
import { IEntity } from './../IEntity'

export enum Gender {
  male = 'male',
  female = 'female',
}

export interface IUserSchema extends IEntity {
  _id?: string
  fullName?: string
  firstName?: string
  lastName?: string
  login?: string
  birthDate?: Date
  gender?: Gender
  address?: IUserAddressSchema[]
  phones?: IUserPhoneSchema[]
  document?: IUserDocumentSchema[]
  isBlocked: boolean
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date
}
