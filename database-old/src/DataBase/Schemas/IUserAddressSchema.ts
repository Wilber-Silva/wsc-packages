export interface IUserAddressSchema {
  city?: string
  country?: string
  zip?: string
  district?: string
}
