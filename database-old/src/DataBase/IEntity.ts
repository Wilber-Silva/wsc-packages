export interface IEntity {
  _id?: string
  createdAt?: Date | string
  updatedAt?: Date | string
  deletedAt?: Date | string
}
