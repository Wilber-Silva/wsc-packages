import { UserPhoneSchema } from './UserPhoneSchema'
import { IUserSchema, Gender } from './../../Schemas'
import { Entity, PrimaryColumn, Column, OneToMany, JoinTable } from 'typeorm'
import { BaseSQLEntity as BaseEntity } from './BaseEntity'
import { UserAddress } from './UserAddress'
import { UserDocumentSchema } from './UserDocumentSchema'

@Entity()
export class UserSQLEntity extends BaseEntity implements IUserSchema {
  @PrimaryColumn()
  _id: string
  @Column()
  fullName: string
  @Column()
  firstName?: string
  @Column()
  lastName?: string
  @Column()
  login: string
  @Column()
  birthDate: Date
  @Column()
  gender: Gender
  @OneToMany((type) => UserAddress, (address) => address.userId)
  @JoinTable()
  address?: UserAddress[]
  @OneToMany((type) => UserPhoneSchema, (phone) => phone.userId)
  @JoinTable()
  phones?: UserPhoneSchema[]
  @OneToMany((type) => UserDocumentSchema, (document) => document.userId)
  @JoinTable()
  document: UserDocumentSchema[]
  @Column()
  isBlocked: boolean = false
  @Column()
  createdAt: Date = new Date()
  @Column()
  updatedAt: Date = null
  @Column()
  deletedAt: Date = null

  constructor(fields: IUserSchema) {
    super()
    Object.assign(this, fields)
  }
}
