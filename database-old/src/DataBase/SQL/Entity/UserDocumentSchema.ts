import {
  IUserDocumentSchema,
  DocumentType,
} from './../../Schemas/IUserDocumentSchema'
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import { BaseSQLEntity as BaseEntity } from './BaseEntity'
import { id } from './../../IDataBaseProvider'
import { UserSQLEntity } from './User'

@Entity()
export class UserDocumentSchema extends BaseEntity
  implements IUserDocumentSchema {
  @PrimaryColumn()
  _id: id
  @ManyToOne((type) => UserSQLEntity, (user) => user._id)
  userId: id
  @Column()
  number: string
  @Column()
  type: DocumentType

  constructor(fields: IUserDocumentSchema) {
    super()
    Object.assign(this, fields)
  }
}
