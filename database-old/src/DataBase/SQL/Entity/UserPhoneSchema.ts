import { IUserPhoneSchema } from './../../Schemas/IUserPhoneSchema'
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import { BaseSQLEntity as BaseEntity } from './BaseEntity'
import { id } from './../../IDataBaseProvider'
import { UserSQLEntity } from './User'

@Entity()
export class UserPhoneSchema extends BaseEntity implements IUserPhoneSchema {
  @PrimaryColumn()
  _id: id
  @ManyToOne((type) => UserSQLEntity, (user) => user._id)
  userId: id
  @Column()
  number: number
  @Column()
  name: string

  constructor(fields: IUserPhoneSchema) {
    super()
    Object.assign(this, fields)
  }
}
