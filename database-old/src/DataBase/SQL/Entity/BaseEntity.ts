import { IEntity } from './../../IEntity'
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm'

@Entity()
export class BaseSQLEntity implements IEntity {
  @PrimaryGeneratedColumn()
  id?: string
  @Column()
  createdAt: Date = new Date()
  @Column()
  updatedAt: Date = null
  @Column()
  deletedAt: Date = null
}
