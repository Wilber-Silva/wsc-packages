import { IUserAddressSchema } from './../../Schemas/IUserAddressSchema'
import { Column, Entity, ManyToOne, PrimaryColumn } from 'typeorm'
import { BaseSQLEntity as BaseEntity } from './BaseEntity'
import { id } from './../../IDataBaseProvider'
import { UserSQLEntity } from './User'

@Entity()
export class UserAddress extends BaseEntity implements IUserAddressSchema {
  @PrimaryColumn()
  _id: id
  @ManyToOne((type) => UserSQLEntity, (user) => user._id)
  userId: id
  @Column()
  city?: string
  @Column()
  country?: string
  @Column()
  zip?: string
  @Column()
  district?: string

  constructor(fields: IUserAddressSchema) {
    super()
    Object.assign(this, fields)
  }
}
