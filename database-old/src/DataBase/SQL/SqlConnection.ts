import { Connection, createConnection } from 'typeorm'
import { IConnection } from './../IConnection'

export enum SQL_DATABASE {
  mysql = 'mysql',
}

interface IConnectionOptions {
  type: SQL_DATABASE
  host?: string
  port?: number
  username?: string
  password?: string
  database: string
  entities: any[]
  synchronize?: boolean
  logging?: boolean
}

export class SqlConnection implements IConnection {
  private options: IConnectionOptions
  connection: Connection
  constructor(options: IConnectionOptions) {
    this.setOptions(options)
  }

  setOptions(options: IConnectionOptions): void {
    if (!options.host) options.host = 'localhost'
    if (!options.port) options.port = 5432
    if (!options.username) options.username = 'root'
    if (!options.password) options.password = ''
    if (typeof options.synchronize === 'undefined') options.synchronize = true
    if (typeof options.logging === 'undefined') options.logging = false

    this.options = options
  }

  getOptions(): IConnectionOptions {
    return this.options
  }

  async connect(): Promise<void> {
    this.connection = await createConnection(this.options)
    await this.connection.connect()
  }
  async close(): Promise<void> {
    await this.connection.close()
  }
}
