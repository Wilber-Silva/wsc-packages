import { SqlConnection, SQL_DATABASE } from './../SqlConnection'
import { UserSQLEntity } from './../Entity/User'
import { UserSQLProvider } from './UserProvider'

const entities = [UserSQLEntity]

export class FactorySQLProvider {
  static defaultSQL = new SqlConnection({
    type: SQL_DATABASE.mysql,
    database: 'test',
    entities,
  })
  static getUserProvider(): UserSQLProvider {
    return new UserSQLProvider(typeof UserSQLEntity, this.defaultSQL)
  }
}
