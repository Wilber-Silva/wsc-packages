import { IDataBaseProvider } from './../../../DataBase/IDataBaseProvider'
import { BaseSQLProvider } from './BaseProvider'

export class UserSQLProvider extends BaseSQLProvider
  implements IDataBaseProvider {}
