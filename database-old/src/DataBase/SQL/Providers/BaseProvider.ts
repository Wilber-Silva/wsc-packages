import { generateId } from './../../../Helpers'
import { SqlConnection } from './../SqlConnection'
import { IEntity } from './../../IEntity'
import { id, IDataBaseProvider } from './../../../DataBase/IDataBaseProvider'
import { Repository, FindOneOptions, FindManyOptions } from 'typeorm'

export abstract class BaseSQLProvider implements IDataBaseProvider {
  protected entity: Repository<any>
  constructor(readonly entityName: string, readonly sql: SqlConnection) {
    // this.entity = this.sql.connection.getRepository(this.entityName)
  }
  async createConnection(): Promise<void> {
    await this.sql.connect()
    this.entity = await this.sql.connection.getRepository(this.entityName)
  }
  async closeConnection(): Promise<void> {
    await this.sql.close()
  }
  protected generateId() {
    return generateId()
  }
  find<T extends IEntity>(options: FindManyOptions): Promise<T[]> {
    return this.entity.find(options)
  }
  list<T extends IEntity>(options: FindManyOptions): Promise<T[]> {
    return this.find<T>(options)
  }
  findOne<T extends IEntity>(options: FindOneOptions): Promise<T> {
    return this.entity.findOne(null, options)
  }
  async findOrCreate<T extends IEntity>(entity: T): Promise<T> {
    const data = await this.entity.findOne(entity._id)
    if (data) {
      return data
    }
    return this.create(data)
  }
  findById<T extends IEntity>(_id: id): Promise<T> {
    return this.entity.findOne(_id)
  }
  async create<T extends IEntity>(data: T, _id?: id): Promise<T> {
    if (!data._id) data._id = this.generateId()
    await this.sql.connection.manager.create(JSON.stringify(data))
    return data
  }
  createMany<T extends IEntity>(collectionToCreate: T[]): Promise<T[]> {
    collectionToCreate = collectionToCreate.map((item) =>
      this.sql.connection.manager.create(
        JSON.stringify({
          ...item,
          _id: item._id ? item._id : this.generateId(),
        }),
      ),
    )
    return Promise.all(collectionToCreate)
  }
  async update<T extends IEntity>(_id: id, data: T): Promise<T> {
    const item = await this.entity.findOne(_id)
    const update = await this.sql.connection.manager.save({ ...item, ...data })
    return !!update ? item : null
  }
  async updateMany<T extends IEntity>(collection: T[]): Promise<boolean> {
    const updatePromises = collection.map((item) => this.update(item._id, item))
    return Promise.all(updatePromises)
      .then(() => true)
      .catch(() => false)
  }
  async delete(_id: id, force?: boolean): Promise<boolean> {
    if (force) {
      const deleted = await this.entity.delete(_id)
      return !!deleted
    }
    const updated = await this.update(_id, { updatedAt: new Date() })
    return !!updated
  }
  deleteMany(ids: id[], force?: boolean): Promise<boolean> {
    const deletePromises = ids.map((_id) => this.delete(_id, force))

    return Promise.all(deletePromises)
      .then(() => true)
      .catch(() => false)
  }
}
