import { FactoryMongoDbProvider } from '@wsc-company/database-mongo'
describe('Test MongoDb Factory', () => {
  let factory = null

  beforeEach(() => {
    factory = FactoryMongoDbProvider.getExampleProvider(process.env.MONGODB)
  })

  it('To be defined', () => {
    expect(factory).toBeDefined()
  })
})
