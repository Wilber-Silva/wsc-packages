module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>/test'],
  setupFiles: ["<rootDir>/test/env.js"],
  transform: {
      '^.+\\.tsx?$': 'ts-jest'
  },
  coverageThreshold: {
      global: {
          branches: 1,
          functions: 1,
          lines: 1,
          statements: 1
      }
  },
  coverageReporters: ['json', 'lcov', 'text', 'clover']
};