import * as admin from 'firebase-admin'

class FirebaseAuth {
  /**
   * Control to auth action
   * @param boolean
   */
  private _isAuth: boolean = false

  /**
   * Method to configure auth params for init connection with Firebase
   *
   * @return void
   */
  public check = (): void => {
    if (!this._isAuth) {
      admin.initializeApp({})
      this._isAuth = true
    }
  }
}

export default new FirebaseAuth() as FirebaseAuth
