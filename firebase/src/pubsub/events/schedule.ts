import { ILogger } from '@wsc-company/types/dist'
import * as functions from 'firebase-functions'
import FirebaseAuth from '../../auth'

export class PubSubScheduleEvent {
  /**
   * Method to get event for PubSub Scheduler
   *
   * @param logger Logger to log application
   * @param rule Rule with datetime to schedule
   * @param event Callback to execute on trigger this event
   * @param timezone Timezone to execute scheduler
   * @return anu
   */
  public static getEvent = (
    logger: ILogger,
    rule: string,
    event: (context: functions.EventContext) => any,
    timezone: string,
    region?: string,
  ): any => {
    FirebaseAuth.check()
    logger.info('Init PubSub Schedule event')
    return functions
      .region(region || 'us-east1')
      .pubsub.schedule(rule)
      .timeZone(timezone)
      .onRun((context: functions.EventContext) => {
        logger.info('Execute PubSub Schedule event')
        return event(context)
      })
  }
}
