import { ILogger } from '@wsc-company/types'
import * as functions from 'firebase-functions'
import FirebaseAuth from '../../auth'

export class PubSubMessageListenEvent {
  /**
   * Method to get event for PubSub Message Listen
   *
   * @param logger Logger to log application
   * @param topic Topic to listen in PubSub
   * @param event Callback to execute on trigger this event
   * @return any
   */
  public static getEvent = (
    logger: ILogger,
    topic: string,
    event: (
      message: functions.pubsub.Message,
      context: functions.EventContext,
    ) => any,
  ) => {
    FirebaseAuth.check()
    logger.info('Init PubSub Message Listen event')
    return functions.pubsub
      .topic(topic)
      .onPublish(
        (
          message: functions.pubsub.Message,
          context: functions.EventContext,
        ) => {
          logger.info('Execute PubSub Message Listen event')
          event(message, context)
          return true
        },
      )
  }
}
