import { ILogger } from '@wsc-company/types'
import * as functions from 'firebase-functions'
import FirebaseAuth from '../../auth'

export class HttpsFunction {
  /**
   * Method to get event for Https Function
   *
   * @param logger Logger to log application
   * @param event Callback to execute on event trigger
   * @return void
   */
  public static getEvent = (
    logger: ILogger,
    event: (
      request: functions.https.Request,
      response: functions.Response,
    ) => any,
  ) => {
    FirebaseAuth.check()
    logger.info('Init Https Function event')
    return functions.https.onRequest(
      (request: functions.https.Request, response: functions.Response) => {
        logger.info('Execute Https Function event')
        logger.debug('Https Function data', {
          request: {
            url: request.baseUrl,
            body: request.rawBody,
            headers: request.rawHeaders,
          },
          response: {
            headers: { ...response.getHeaders() },
            statusCode: response.statusCode,
          },
        })
        event(request, response)
      },
    )
  }
}
