import { Settings } from '@google-cloud/firestore'
import { SettingsFireBaseDefault, SettingsFireBaseLocal } from './'

const { NODE_ENV } = process.env

export class FactoryConnection {
  /**
   * True if is localhost
   * @param: isLocal: boolean
   */
  public static isLocal: boolean =
    NODE_ENV === 'local' || NODE_ENV === 'localhost'

  /**
   * Return default settings to connection on db
   * @return Settings
   */
  public static getDefault(): Settings {
    if (this.isLocal) return new SettingsFireBaseLocal()

    return new SettingsFireBaseDefault()
  }
}
