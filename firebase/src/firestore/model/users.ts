import { BaseModelFirestore } from '.'
import { IUserSchema, IUserModel } from '@wsc-company/types'
import { Factory as SchemaFactory } from '@wsc-company/schemas'
import { Settings } from '@google-cloud/firestore'

export class Users extends BaseModelFirestore implements IUserModel {
  /**
   * Method to construct instance of model Users
   *
   * @return void
   */
  constructor(
    /**
     * @param connection: Settings
     */
    connection: Settings,
  ) {
    super('users', connection, (document) => {
      return SchemaFactory.getUserSchema(document)
    })
  }

  /**
   * Method find user by document number (cpf)
   * @param cpf user document number
   */
  findByCpf(cpf: string): Promise<IUserSchema> {
    return this.find<IUserSchema>({
      whereAnd: [
        {
          field: 'cpf',
          op: '==',
          value: cpf,
        },
      ],
      limit: 1,
    })[0]
  }
}

export default Users
