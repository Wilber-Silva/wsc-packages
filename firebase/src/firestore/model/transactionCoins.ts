import {
  Factory as SchemaFactory,
  TransactionCoinsSchema,
} from '@wsc-company/schemas'
import {
  ITransactionCoinDTO,
  ITransactionCoinsSchema,
  ITransactionCoinsModel,
  COINS_TYPE_OPERATION,
} from '@wsc-company/types'
import { Settings } from '@google-cloud/firestore'
import { BaseModelFirestore } from './baseModelFirestore'

export class TransactionCoins
  extends BaseModelFirestore
  implements ITransactionCoinsModel {
  /**
   * Method to construct instance of model Groups
   *
   * @return void
   */
  constructor(
    /**
     * @param connection: Settings
     */
    connection: Settings,
  ) {
    super('transactionCoins', connection, (document) => {
      return SchemaFactory.getTransactionCoinsSchema(document)
    })
  }
  /**
   * Calculate expire coin date
   * @returns string
   */
  private calculateExpire() {
    const date = new Date()
    return this.dateToString(new Date(date.setMonth(date.getMonth() + 1)))
  }
  /**
   * Method to create a credit operation
   * @param params ITransactionCoinDTO
   * @returns string: id of created
   */
  credit(params: ITransactionCoinDTO): Promise<string> {
    const transaction: ITransactionCoinsSchema = {
      type: COINS_TYPE_OPERATION.credit,
      coins: params.coins,
      description: params.description,
      userId: params.userId,
      active: true,
      expiredAt: this.calculateExpire(),
    }
    return super.create(transaction)
  }
  /**
   * Method to create a debit operation
   * @param params ITransactionCoinDTO
   * @returns string: id of created
   */
  debit(params: ITransactionCoinDTO): Promise<string> {
    const transaction: ITransactionCoinsSchema = {
      type: COINS_TYPE_OPERATION.debit,
      coins: -1 * params.coins,
      description: params.description,
      userId: params.userId,
      active: true,
      expiredAt: this.calculateExpire(),
    }
    return super.create(transaction)
  }
  /**
   * Method to get total coins
   * @param userId string
   * @returns number: total coins
   */
  async totalCoins(userId: string): Promise<number> {
    const transactions = await this.find<TransactionCoinsSchema>({
      whereAnd: [
        {
          field: 'userId',
          op: '==',
          value: userId,
        },
        {
          field: 'active',
          op: '==',
          value: true,
        },
      ],
    })

    let coins = 0

    for (const trans of transactions) {
      coins = coins + trans.coins
    }
    return coins
  }
}
