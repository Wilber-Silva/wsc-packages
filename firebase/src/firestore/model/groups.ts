import { BaseModelFirestore } from './'
import { IGroupsSchema, IGroupsModel } from '@wsc-company/types'
import { Factory as SchemaFactory } from '@wsc-company/schemas'
import { Settings } from '@google-cloud/firestore'

export class Groups extends BaseModelFirestore implements IGroupsModel {
  /**
   * Method to construct instance of model Groups
   *
   * @return void
   */
  constructor(
    /**
     * @param connection: Settings
     */
    connection: Settings,
  ) {
    super('groups', connection, (document) => {
      return SchemaFactory.getGroupSchema(document)
    })
  }

  /**
   * Method to find document by tag
   *
   * @param tag Tag to find
   * @return Promise<GroupsSchema>
   */
  async findByTag(tag: string): Promise<IGroupsSchema> {
    const collection = await this.find<IGroupsSchema>({
      whereAnd: [
        {
          field: 'tag',
          op: '==',
          value: tag,
        },
      ],
      limit: 1,
    })
    if (!collection || collection.length === 0) return null
    const group = collection[0]
    return this._factoryModel(group)
  }
}

export default Groups
