import { FactoryConnection } from './connection'
import { Goals, Groups, Users, TransactionCoins } from './'
import { Settings } from '@google-cloud/firestore'

export class FactoryModel {
  /**
   * @param defaultConnection: Firebase connection instance
   */
  private static defaultConnection: Settings = FactoryConnection.getDefault()

  /**
   * Get model instance
   * @returns Goals
   */
  public static getGoals(): Goals {
    return new Goals(this.defaultConnection)
  }
  /**
   * Get model instance
   * @returns Groups
   */
  public static getGroups(): Groups {
    return new Groups(this.defaultConnection)
  }
  /**
   * Get model instance
   * @returns Users
   */
  public static getUsers(): Users {
    return new Users(this.defaultConnection)
  }
  /**
   * Get model instance
   * @returns TransactionCoins
   */
  public static getTransactionCoins(): TransactionCoins {
    return new TransactionCoins(this.defaultConnection)
  }
}
