import { BaseModelFirestore } from './'
import { IGoalsSchema, IGoalsModel } from '@wsc-company/types'
import { Factory as SchemaFactory } from '@wsc-company/schemas'
import { Settings } from '@google-cloud/firestore'

export class Goals extends BaseModelFirestore implements IGoalsModel {
  /**
   * Method to construct instance of model Goals
   *
   * @return void
   */
  constructor(
    /**
     * @param connection: Settings
     */
    connection: Settings,
  ) {
    super('goals', connection, (document) => {
      return SchemaFactory.getGoalsSchema(document)
    })
  }

  /**
   * Method to find document by group and order
   *
   * @param groupId Group of document
   * @param order Order of document
   * @return Promise<GoalsSchema>
   */
  public findByGroupAndOrder = async (
    groupId: string,
    order: number,
  ): Promise<IGoalsSchema> => {
    const collection = await this.find({
      whereAnd: [
        {
          field: 'groupId',
          op: '==',
          value: groupId,
        },
        {
          field: 'order',
          op: '==',
          value: order,
        },
      ],
      limit: 1,
    })
    const goal = this._factoryModel(collection[0])
    if (goal.deletedAt) return null
    return goal
  }

  /**
   * Method to get next document
   *
   * @param groupId Id of grupo
   * @param order Order of documents
   * @return Promise<IGoalsSchema[]>
   */
  public next = async (
    groupId: string,
    order: number,
  ): Promise<IGoalsSchema[]> => {
    const collection = await this.find({
      whereAnd: [
        {
          field: 'groupId',
          op: '==',
          value: groupId,
        },
        {
          field: 'order',
          op: '>',
          value: order,
        },
      ],
    })
    return collection.map((item) => this._factoryModel(item))
  }

  /**
   * Create Goals
   * @override Super
   * @params IGoalsSchema
   * @return string
   */
  create = (params: IGoalsSchema, id?: string): Promise<string> => {
    if (typeof params.publishAt === 'string')
      params.publishAt = new Date(params.publishAt)
    if (typeof params.expiredAt === 'string')
      params.expiredAt = new Date(params.expiredAt)

    return super.create(params, id)
  }
}

export default Goals
