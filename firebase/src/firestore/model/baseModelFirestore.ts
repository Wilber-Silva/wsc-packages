import {
  IDelete,
  IFindWhereFirestore,
  IModel,
  IBaseSchema,
  IUpdated,
  IOptionsQueryFirestore,
  IDeleted,
} from '@wsc-company/types'
import {
  CollectionReference,
  DocumentData,
  DocumentSnapshot,
  Firestore,
  QueryDocumentSnapshot,
  QuerySnapshot,
  WriteBatch,
  Settings,
} from '@google-cloud/firestore'
import { v4 as uuidv4 } from 'uuid'

export abstract class BaseModelFirestore implements IModel {
  protected readonly firestore: Firestore
  protected readonly _factoryCollectionFirestore: () => CollectionReference
  protected readonly batch: WriteBatch
  protected readonly dateNow: Date

  /**
   * Default condition to all cases
   * @param IFindWhereFirestore[]
   */
  private readonly _defaultWhereAnd: IFindWhereFirestore[] = [
    {
      field: 'deletedAt',
      op: '==',
      value: null,
    },
  ]

  /**
   * Method to construct instance of Model
   *
   * @param collectionName Name of collection
   * @param _factoryModel Factory to create model instance
   * @return void
   */
  constructor(
    public readonly collectionName: string,
    protected readonly _settings: Settings,
    protected _factoryModel: (document) => any,
  ) {
    this.firestore = new Firestore(this._settings)
    this.batch = this.firestore.batch()
    this._factoryCollectionFirestore = (): CollectionReference => {
      return this.firestore.collection(this.collectionName)
    }
    this.dateNow = new Date()
  }
  /**
   * Method to find record
   *
   * @param params Param to apply in collection
   * @return Promise<T[]>
   */
  public find = async <T>(options?: IOptionsQueryFirestore): Promise<T[]> => {
    if (!options.trashed) {
      const deletedAt: IFindWhereFirestore = {
        field: 'deletedAt',
        op: '==',
        value: null,
      }
      if (options.whereAnd) options.whereAnd.push(deletedAt)
      else options.whereAnd = [deletedAt]
    }
    const snapshot = await this._find(options)
    return this.convertToCollection<T>(snapshot)
  }

  /**
   * Method to test if document exists
   *
   * @param filters IFindWhereFirestore[]
   * @return Promise<boolean>
   */
  public exists = async <T>(
    filters: IFindWhereFirestore[],
    able?: boolean,
  ): Promise<T | boolean> => {
    const alreadyExists = await this.find<T>({
      whereAnd: filters,
      limit: 1,
    })[0]

    if (typeof alreadyExists === 'undefined') return false
    if (able) return alreadyExists

    return !!alreadyExists
  }

  /**
   * Method to find by id and populate model
   *
   * @param id Id of document
   * @return Promise<T>
   */
  public async findById<T>(id: string): Promise<T> {
    const collection = this._factoryCollectionFirestore()
    const snapshot = await collection.doc(id).get()
    const item = await this.convertToObject<IBaseSchema>(snapshot)
    if (item && item.deletedAt) return null
    return this._factoryModel(item)
  }

  /**
   * Method to create document
   *
   * @param id ID of document
   * @param params Data of document
   * @return Promise<string>
   */
  public create = async (params: IBaseSchema, id?: string): Promise<string> => {
    if (!id) id = this.generateId()
    const document = this.toFirestore(params)
    document.createdAt = this.dateToString()
    document.updatedAt = null
    document.deletedAt = null
    const collection = this._factoryCollectionFirestore()
    const created = await collection.doc(id).set(document)
    if (created) return id
  }

  /**
   * Method to update document
   *
   * @param id ID of document
   * @param item Data of document
   * @return Promise<boolean>
   */
  public update = async (id: string, item: IBaseSchema): Promise<boolean> => {
    const document = this.toFirestore(item)
    document.updatedAt = this.dateToString()
    const collection = this._factoryCollectionFirestore()
    await collection.doc(id).set(document, { merge: true })
    return true
  }

  /**
   * Method to create many document
   *
   * @param collection: IBaseSchema
   * @return Promise<string[]>
   */
  public createMany = async (collection: IBaseSchema[]): Promise<string[]> => {
    const createdIds = []
    const _collection = this._factoryCollectionFirestore()
    for (const item of collection) {
      const document = this.toFirestore(item)
      document.createdAt = this.dateToString()
      document.updatedAt = null
      document.deletedAt = null
      const id = this.generateId()
      const ref = _collection.doc(id)
      this.batch.set(ref, document)
      createdIds.push(id)
    }
    const created = await this.batch.commit()
    if (!created) return []
    return createdIds
  }

  /**
   * Method to update many document
   *
   * @param collection Collection of documents
   * @return Promise<IUpdated[]>
   */
  public updateMany = async (
    collection: IBaseSchema[],
  ): Promise<IUpdated[]> => {
    const updated = []
    const _collection = this._factoryCollectionFirestore()
    for (const item of collection) {
      const _id = item._id
      const document = this.toFirestore(item)
      const ref = _collection.doc(_id)
      this.batch.update(ref, document)
      updated.push({
        _id,
        updated: true,
      })
    }
    const updateResult = await this.batch.commit()
    if (!updateResult) return []
    return updated
  }
  /**
   * Method to delete many document
   *
   * @param collection Collection of documents
   * @return Promise<IDeleted[]>
   */
  async deleteMany(params: IDelete[]): Promise<IDeleted[]> {
    const deleted = []
    const _collection = this._factoryCollectionFirestore()
    for (const item of params) {
      const _id = item.id
      const ref = _collection.doc(_id)
      if (item.force) this.batch.delete(ref)
      else
        this.batch.update(ref, {
          updatedAt: this.dateToString(),
          deletedAt: this.dateToString(),
        })
      deleted.push({
        _id,
        deleted: true,
      })
    }
    const updateResult = await this.batch.commit()
    if (!updateResult) return []
    return deleted
  }

  /**
   * Method to delete document
   *
   * @param params:IDelete
   * @return Promise<boolean>
   */
  public delete = async ({ id, force }: IDelete): Promise<boolean> => {
    const collection = this._factoryCollectionFirestore()
    if (force) {
      await collection.doc(id).delete()
      return true
    }
    await collection.doc(id).set(
      {
        updatedAt: this.dateToString(),
        deletedAt: this.dateToString(),
      },
      { merge: true },
    )
    return true
  }

  /**
   * Method to convert document in object
   *
   * @param cursor Cursor of data
   * @return Promise<any>
   */
  public convertToObject = async <T>(
    cursor: DocumentData | DocumentSnapshot,
  ): Promise<T | IBaseSchema> => {
    if (!cursor) return null
    if (!cursor.data()) return null

    return this._factoryModel({
      _id: cursor.id,
      ...cursor.data(),
    })
  }

  /**
   * Method to convert resource of data source into collection of records
   *
   * @param cursor QuerySnapshot | Record<string, any>
   * @return Promise<any[]>
   */
  protected convertToCollection = async <T>(
    cursor: QuerySnapshot | Record<string, any>,
  ): Promise<T[]> => {
    const collection = []
    if (!cursor || cursor.empty) return []
    cursor.forEach((element: QueryDocumentSnapshot) => {
      collection.push(
        this._factoryModel({
          _id: element.id,
          ...element.data(),
        }),
      )
    })
    return collection
  }

  /**
   * Method to format date
   *
   * @param date Date to format
   * @return string
   */
  protected dateToString = (date?: Date): string => {
    if (!date) return this.dateNow.toISOString()

    return date.toISOString()
  }

  /**
   * Method to generate UUID
   *
   * @return string
   */
  protected generateId = (): string => {
    return uuidv4()
  }

  /**
   * Method to make find in Filestore
   *
   * @param options Options to apply in query
   * @return Promise<QuerySnapshot<DocumentData>>
   */
  private _find = (
    options?: IOptionsQueryFirestore,
  ): Promise<QuerySnapshot<DocumentData>> => {
    const collection = this._factoryCollectionFirestore()
    const whereAnd = [...(options.whereAnd || []), ...this._defaultWhereAnd]
    if (whereAnd) {
      whereAnd.map((where) => {
        collection.where(where.field, where.op, where.value)
      })
    }
    if (options.limit) {
      collection.limit(options.limit)
    }
    return collection.get()
  }

  /**
   * Method to remove ID of document
   *
   * @param model Model with data
   * @return DocumentData
   */
  private toFirestore = (model: IBaseSchema): DocumentData => {
    delete model._id
    return { ...model }
  }
}
