import { ILogger } from '@wsc-company/types'
import * as functions from 'firebase-functions'

type context = functions.EventContext
type change = functions.Change<functions.firestore.DocumentSnapshot>
type queryDocumentSnapshot = functions.firestore.QueryDocumentSnapshot

export interface IonWrite {
  change: change
  context: context
}
export interface IonCreate {
  querySnapshot: queryDocumentSnapshot
  context: context
}

export class FirestoreDocumentCreateListen {
  /**
   * Method to get instance for listen create document in Firestore
   *
   * @param document Document to listen
   * @param event Callback to execute on create document
   * @return any
   */
  public static getEvent = function (
    logger: ILogger,
    event: (params: IonCreate | IonWrite) => any,
    params: {
      document: string
      onWrite?: boolean
      onCreate?: boolean
      onUpdate?: boolean
    },
  ): any {
    logger.info(`Init Firestore Document ${params.document} Observer`)
    if (params.onCreate) {
      return functions.firestore.document(params.document).onCreate(
        // tslint:disable-next-line: no-shadowed-variable
        (querySnapshot: queryDocumentSnapshot, context: context) => {
          logger.info('Execute Firestore Document Create event')
          return event({ querySnapshot, context })
        },
      )
    } else if (params.onWrite) {
      return (
        functions.firestore
          .document(params.document)
          // tslint:disable-next-line: no-shadowed-variable
          .onWrite((change: change, context: context) => {
            logger.info('Execute Firestore Document Write event')
            logger.debug('Before write', change.before.data())
            logger.debug('After write', change.after.data())
            return event({ change, context })
          })
      )
    } else if (params.onUpdate) {
      return (
        functions.firestore
          .document(params.document)
          // tslint:disable-next-line: no-shadowed-variable
          .onUpdate((change: change, context: context) => {
            logger.info('Execute Firestore Document Update event')
            logger.debug('Before write', change.before.data())
            logger.debug('After write', change.after.data())
            return event({ change, context })
          })
      )
    }
  }
}
