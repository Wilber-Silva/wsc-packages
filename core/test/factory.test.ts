import { Factory as LoggerFactory } from '@wsc-company/logger'
import { IHandler, IApplication } from '@wsc-company/types'
import { Application, Factory } from '../src'

describe('Factory Test', () => {
  let application: IApplication = null
  const handler: IHandler = {
    dispach: null,
    getArgs: null,
    stop: null,
    getMiddlewaresDefaut: null,
  }

  beforeEach(() => {
    application = Factory.getApplication(
      'application to test factory',
      handler,
      LoggerFactory.getLogger('debug', 'file', '/tmp/test.log'),
    )
  })

  // Test get Application
  it('Test get Application', () => {
    expect(application).toBeInstanceOf(Application)
  })
})
