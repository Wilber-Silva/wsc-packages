import { Factory } from '../src'
import { IApplication, IHandler, ILogger } from '@wsc-company/types'

describe('Application Test', () => {
  const applicationName = 'application to unit test'
  let application: IApplication
  const handler: IHandler = {
    dispach: () => null,
    getArgs: () => null,
    stop: () => null,
    getMiddlewaresDefaut: () => null,
  }
  const logger: ILogger = {
    debug: () => null,
    error: () => null,
    exception: () => null,
    fatal: () => null,
    info: () => null,
    trace: () => null,
    warning: () => null,
  }

  beforeEach(() => {
    application = Factory.getApplication(applicationName, handler, logger)
  })

  // Test constructor and name of application
  it('Test constructor and name of application', () => {
    expect(application.name).toBe(applicationName)
  })

  // Test before dispach event
  it('Test before dispach event', () => {
    let triggerExecuted = false
    try {
      application.beforeDispach(() => {
        triggerExecuted = true
        return true
      })
      application.dispach()
      expect(triggerExecuted).toBe(true)
    } catch (err) {
      throw err
    } finally {
      application.stop()
    }
  })

  // Test exception on before dispach event
  it('Test exception on before dispach event', () => {
    let hasException = false
    try {
      application.beforeDispach(() => {
        throw new Error('test exception after event')
      })
      application.dispach()
    } catch (err) {
      hasException = true
    } finally {
      expect(hasException).toBe(true)
      application.stop()
    }
  })

  // Test after dispach event
  it('Test after dispach event', () => {
    let triggerExecuted = false
    try {
      application.afterDispach(() => {
        triggerExecuted = true
        return true
      })
      application.dispach()
      expect(triggerExecuted).toBe(true)
    } catch (err) {
      throw err
    } finally {
      application.stop()
    }
  })

  // Test exception on after dispach event
  it('Test exception on after dispach event', () => {
    let hasException = false
    try {
      application.afterDispach(() => {
        throw new Error('test exception after event')
      })
      application.dispach()
    } catch (err) {
      hasException = true
    } finally {
      expect(hasException).toBe(true)
      application.stop()
    }
  })

  // Test before and after events
  it('Test before and after events', () => {
    let hasBefore = false
    let hasAfter = false
    try {
      application.beforeDispach(() => {
        hasBefore = true
        return true
      })
      application.afterDispach(() => {
        hasAfter = true
        return true
      })
      application.dispach()
      expect(hasBefore).toBe(true)
      expect(hasAfter).toBe(true)
    } catch (err) {
      throw err
    } finally {
      application.stop()
    }
  })

  // Test can execute dispach without before event
  it('Test can execute dispach without before event', () => {
    let hasBeforeEvent = false
    try {
      application.beforeDispach(null)
      application.afterDispach(() => {
        hasBeforeEvent = true
        return true
      })
      application.dispach()
      expect(hasBeforeEvent).toBe(true)
    } catch (err) {
      throw err
    } finally {
      application.stop()
    }
  })

  // Test dont can execute dispach
  it('Test dont can execute dispach', () => {
    let canDispach = true
    try {
      application.beforeDispach(() => {
        return false
      })
      application.dispach()
    } catch (err) {
      canDispach = false
    } finally {
      application.stop()
    }
    expect(canDispach).toBe(false)
  })

  // 'Test get handler'
  it('Test get handler', () => {
    const instance = application.getHandler()
    expect(typeof instance.dispach === 'function').toBe(true)
  })
})
