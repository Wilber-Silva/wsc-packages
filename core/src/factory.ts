import { IApplication, IHandler, ILogger } from '@wsc-company/types'
import { Application } from '.'

export class Factory {
  /**
   * Logger to use in application
   * @param ILogger
   */
  public static logger: ILogger = null

  /**
   * Method static to get instance of application
   *
   * @param name Name of application
   * @param handler Handler to dispach request
   * @param logger Logger to log application
   * @return IApplication
   */
  public static getApplication = (
    name: string,
    handler: IHandler,
    logger: ILogger,
  ): IApplication => {
    return new Application(name, handler, logger)
  }
}
