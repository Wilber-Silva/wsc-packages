import { IApplication, IHandler, ILogger } from '@wsc-company/types'

export class Application implements IApplication {
  /**
   * Callback to execute after dispach
   * @param (application: IApplication) => boolean
   */
  private _afterDispach: (application: IApplication) => boolean = null

  /**
   * Callback to execute before dispach
   * @param (application: IApplication) => boolean
   */
  private _beforeDispach: (application: IApplication) => boolean = null

  /**
   * Method to construct instance of Application
   *
   * @param _handler Handler of application for send to Express or Google Function
   * @param _logger Logger to log application
   * @return void
   */
  constructor(
    /**
     * Name of application
     * @param string
     */
    public name: string,

    /**
     * Handler to send daemon
     * @param IHandler
     */
    private _handler: IHandler,

    /**
     * Logger to log application
     * @param ILogger
     */
    private _logger: ILogger,
  ) {}

  /**
   * Method to configure trigger after dispach
   *
   * @param callback Callback to call after dispach
   * @return void
   */
  public afterDispach = (
    callback: (application: IApplication) => boolean,
  ): void => {
    this._afterDispach = callback
  }

  /**
   * Method to configure trigger before dispach
   *
   * @param callback Callback to call after dispach
   * @return void
   */
  public beforeDispach = (
    callback: (application: IApplication) => boolean,
  ): void => {
    this._beforeDispach = callback
  }

  /**
   * Method to dispach daemon to Express or Google Funtions
   *
   * @throws Error on execute handler trigger after, before or dispach handler
   * @return void
   */
  public dispach = (): void => {
    try {
      this._logger.debug('try dispach request', {
        handler: this._handler.constructor.name,
      })
      if (this.canDispach()) {
        this._handler.dispach(this)
        this._logger.debug('success dispach request', {
          handler: this._handler.constructor.name,
        })
        if (typeof this._afterDispach === 'function') {
          this._logger.debug('Execute trigger after dispach', {
            afterDispach: this._afterDispach,
          })
          this._afterDispach(this)
        }
      } else {
        this._logger.error('Error on try dispach request', {
          beforeDispach: this._beforeDispach,
          afterDispach: this._afterDispach,
        })
        throw new Error('Error on try dispach request')
      }
    } catch (err) {
      this._logger.exception(err)
      throw err
    }
  }

  /**
   * Method to get instance of Handler
   *
   * @return IHandler
   */
  public getHandler = (): IHandler => {
    return this._handler
  }

  /**
   * Method to stop execution of handler
   *
   * @return void
   */
  public stop = (): void => {
    this._handler.stop()
  }

  /**
   * Method to test if can dispach
   *
   * @return boolean
   */
  private canDispach = (): boolean => {
    if (typeof this._beforeDispach === 'function') {
      this._logger.debug('Execute trigger before dispach', {
        beforeDispach: this._beforeDispach,
      })
      return this._beforeDispach(this)
    }
    return true
  }
}
