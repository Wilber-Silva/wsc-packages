import { FactoryRepository } from './../src/factory'
import { ExampleDatabaseRepository } from 'src'
import { IExampleSchema } from '@wsc-company/types/dist'

describe('Test Example Repository', () => {
  let repository: ExampleDatabaseRepository = null
  const name = 'repo-jest-test'

  beforeEach(() => {
    repository = FactoryRepository.getExampleRepository()
  })

  it('Defined', () => {
    expect(repository).toBeDefined()
  })

  it('Create and delete', async () => {
    const created = await repository.create<IExampleSchema>({ name })
    expect(created.name).toBe(name)

    const deleted = await repository.delete(created._id)
    expect(deleted).toBe(true)
  })
})
