import { IBaseSchema, IDatabaseRepository } from '@wsc-company/types'
import { BaseMongoDbProvider } from '@wsc-company/database'

export class BaseDatabaseRepository implements IDatabaseRepository {
  /**
   * Default deletes rule
   */
  private softDelete = false
  constructor(
    /**
     * Database provider instance
     */
    readonly _provider: BaseMongoDbProvider,
  ) {}
  /**
   * Find items on collections
   * @param data Item values to make a query
   */
  async find<T extends IBaseSchema>(data: T): Promise<T[]> {
    throw new Error('Method not implemented.')
  }
  /**
   * Find items on collections
   * @param data Item values to make a query
   */
  async list<T extends IBaseSchema>(data: T): Promise<T[]> {
    throw new Error('Method not implemented.')
  }
  /**
   * Find one item
   * @param data Item values to make a query
   */
  async findOne<T extends IBaseSchema>(data: T): Promise<T> {
    throw new Error('Method not implemented.')
  }
  /**
   * Find or create item
   * @param data Item values to make a query
   */
  async findOrCreate<T extends IBaseSchema>(data: T): Promise<T> {
    await this._provider.createConnection()
    const founded = await this.findOne<T>(data)
    if (!data) {
      const created = await this.create<T>(data)
      return created
    }

    return founded
  }
  /**
   * Find or create item
   * @param data Item values to make a query
   */
  async findById<T extends IBaseSchema>(_id: string): Promise<T> {
    await this._provider.createConnection()
    return this._provider.findById<T>(_id)
  }
  /**
   * Create item
   * @param data Data to create
   */
  async create<T extends IBaseSchema>(data: T): Promise<T> {
    await this._provider.createConnection()
    return this._provider.create<T>(data)
  }
  /**
   * Create items
   * @param data Data to create
   */
  async createMany<T extends IBaseSchema>(
    collectionToCreate: T[],
  ): Promise<T[]> {
    await this._provider.createConnection()
    return this._provider.createMany(collectionToCreate)
  }
  /**
   * Update item
   * @param _id item reference
   * @param data Data to update
   */
  async update<T extends IBaseSchema>(_id: string, data: T): Promise<T> {
    await this._provider.createConnection()
    return this._provider.update<T>(_id, data)
  }
  /**
   * Update items
   * @param data Data to update
   */
  async updateMany<T extends IBaseSchema>(data: T[]): Promise<boolean> {
    await this._provider.createConnection()
    return this._provider.updateMany(data)
  }
  /**
   * Delete item
   * @param _id item reference
   * @param force permanent delete
   */
  async delete(_id: string, force?: boolean): Promise<boolean> {
    await this._provider.createConnection()
    if (typeof force !== 'undefined') {
      return this._provider.delete(_id, force)
    } else {
      return this._provider.delete(_id, this.softDelete)
    }
  }
  /**
   * Delete items
   * @param ids items reference
   * @param force permanent delete
   */
  async deleteMany(ids: string[], force?: boolean): Promise<boolean> {
    await this._provider.createConnection()
    if (typeof force !== 'undefined') {
      return this._provider.deleteMany(ids, force)
    } else {
      return this._provider.deleteMany(ids, this.softDelete)
    }
  }
}
