import { IDatabaseRepository } from '@wsc-company/types'
import { BaseDatabaseRepository } from './BaseDatabaseRepository'

export class ExampleDatabaseRepository
  extends BaseDatabaseRepository
  implements IDatabaseRepository {}
