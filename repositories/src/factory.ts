import { FactoryMongoDbProvider } from '@wsc-company/database/dist/src'
import { ExampleDatabaseRepository } from './ExampleDatabaseRepository'

export class FactoryRepository {
  static getExampleRepository() {
    const dbExample =
      'mongodb+srv://will:413105839ww@cluster0.l4wun.mongodb.net/test?retryWrites=true&w=majority'

    return new ExampleDatabaseRepository(
      FactoryMongoDbProvider.getExampleProvider(dbExample),
    )
  }
}
