# StartSe packages #

 Pacotes internos startse

## Get Started ##

```bash
    npm install @wsc-company/${package-name}
```

Para desenvolver uma atualização para um pacote e não precisar fazer a publicação do pacote para testar pode ser usado o link simbolico

```bash
    npm link ${package-path}
```

Assim sempre que for executado o build o link irá pegar a atualização do path que lhe for passado.

E para não ter problema com o link é interessante instalar as dependencias de todos os pacotes e deixar todos com o link simbilico em tempo de desenvolvimento

```bash
    npm run install-all && npm run link-all
```

### Dependencies ###

Package name | Dependence
------------ | ------:
[types](https://github.com/StartSe/packages/tree/master/types)       | **none**
[logger](https://github.com/StartSe/packages/tree/master/logger)     | ° **types**
[service](https://github.com/StartSe/packages/tree/master/service)      | ° **types**
[database-mongo](https://github.com/StartSe/packages/tree/master/database-mongo) | ° **types**
[database](https://github.com/StartSe/packages/tree/master/database) | ° **database-mongo**
[core](https://github.com/StartSe/packages/tree/master/core)        | ° **types** ° **logger**

### Publish ###

```bash
    npm run install-all && npm run link-all && npm run publish-all
```

or

```bash
    npm run publish-packages
```
