import { ILogger } from '@wsc-company/types'
import * as log4js from 'log4js'
import { Logger } from '.'
import { LoggerInterfaceLog4Js } from './interfaces'

export class Factory {
  /**
   * Instance singleton of Logger
   * @param ILogger
   */
  private static logger: ILogger = null

  /**
   * @return ILogger
   */
  public static getLogger = (
    level: string,
    type: string,
    file?: string,
  ): ILogger => {
    return new Logger(Factory.getLoggerLog4Js(level, type, file))
  }

  /**
   * Method to get Logger Interface to Log4Js
   *
   * @return ILogger
   */
  public static getLoggerLog4Js = (
    level: string,
    type: string,
    file?: string,
  ): ILogger => {
    if (!Factory.logger) {
      Factory.logger = new Logger(Factory.getInstanceLog4Js(level, type, file))
    }
    return Factory.logger
  }

  /**
   * MetHod to get Logger interface to Log4js
   *
   * @param level Level of log
   * @param type Type of log
   * @param file File to save log
   * @return ILogger
   */
  private static getInstanceLog4Js = (
    level: string,
    type: string,
    file?: string,
  ): ILogger => {
    log4js.configure({
      appenders: {
        console: { type: 'console' },
        file: { type: 'file', filename: file },
      },
      categories: { default: { appenders: [type], level } },
    })
    return new LoggerInterfaceLog4Js(log4js.getLogger())
  }
}
