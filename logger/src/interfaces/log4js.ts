import log4js from 'log4js'
import { ILogger } from '@wsc-company/types'

export class LoggerInterfaceLog4Js implements ILogger {
  /**
   * Method to construct instance of log4js
   *
   * @param _log4js Interface to use logs
   * @return void
   */
  constructor(
    /**
     * Instance of log4js
     * @param log4js.Logger
     */
    private _log4js: log4js.Logger,
  ) {}

  /**
   * Method to log trace
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public trace = (message: string, args: any[]): void =>
    this._log4js.trace(message, args)

  /**
   * Method to log debug
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public debug = (message: string, args: any[]): void =>
    this._log4js.debug(message, args)

  /**
   * Method to log info
   *
   * @param message message to log
   * @return void
   */
  public info = (message: string, args: any[]): void =>
    this._log4js.info(message, args)

  /**
   * Method to log warning
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public warning = (message: string, args: any[]): void =>
    this._log4js.warn(message, args)

  /**
   * Method to log error
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public error = (message: string, args: any[]): void =>
    this._log4js.error(message, args)

  /**
   * Method to log exception
   *
   * @param err Exception to parse and log
   * @return void
   */
  public exception = (err: Error): void => this._log4js.error(err.message, err)

  /**
   * Method to log fatal
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public fatal = (message: string, args: any[]): void =>
    this._log4js.fatal(message, args)
}
