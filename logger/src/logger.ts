import { ILogger } from '@wsc-company/types'

export class Logger implements ILogger {
  /**
   * Method to construct instance of Logger
   *
   * @param _interface Interface to use logs
   * @return void
   */
  constructor(
    /**
     * Instance of log4js
     * @param ILoggerInterface
     */
    private _interface: ILogger,
  ) {}

  /**
   * Method to log trace
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public trace = (message: string, args?: any): void =>
    this._interface.trace(message, args)

  /**
   * Method to log debug
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public debug = (message: string, args?: any): void =>
    this._interface.debug(message, args)

  /**
   * Method to log info
   *
   * @param message message to log
   * @return void
   */
  public info = (message: string, args?: any): void =>
    this._interface.info(message, args)

  /**
   * Method to log warning
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public warning = (message: string, args?: any): void =>
    this._interface.warning(message, args)

  /**
   * Method to log error
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public error = (message: string, args?: any): void =>
    this._interface.error(message, args)

  /**
   * Method to log exception
   *
   * @param err Exception to parse and log
   * @return void
   */
  public exception = (err: Error): void => this._interface.exception(err)

  /**
   * Method to log fatal
   *
   * @param message Message to log
   * @param args any[]
   * @return void
   */
  public fatal = (message: string, args?: any): void =>
    this._interface.fatal(message, args)
}
