# Logger #

O pacote da StartSe para logs é uma abstração para a geração de logs de sistema, como console e file log.

## External dependencies ##

Usamos o log4js para gerar os logs dentro da camada abstrata
<https://www.npmjs.com/package/log4js>

## Internal dependencies ##

* @wsc-company/types

## Methods usages ##

~~~javascript
    // pass - LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE
    const logger = Factory.getLogger('debug', 'console', '')
~~~

* Debug

~~~javascript
    logger.debug('example debug', [ { param: 'debug' } ])
    /*
        Result

        [2020-09-18T15:59:00.293] [DEBUG] default - example debug [ { param: 'debug' } ]
    */
~~~

* Info

~~~javascript
    logger.info('example info', [ { param: 'info' } ])
    /*
        Result

        [2020-09-18T15:59:00.306] [INFO] default - example info [ { param: 'info' } ]
    */
~~~

* Warn

~~~javascript
    logger.warning('example warning', [ { param: 'warning' } ])
    /*
        Result

        [2020-09-18T15:59:00.307] [WARN] default - example warning [ { param: 'warning' } ]
    */
~~~

* Error

~~~javascript
    logger.error('example error', [ { param: 'error' } ])
    /*
        Result

        [2020-09-18T15:59:00.307] [ERROR] default - example error [ { param: 'error' } ]
    */
~~~

* Exception

~~~javascript
    logger.exception(new Error('example exception'))
    /*
        Result

        [2020-09-18T15:59:00.308] [ERROR] default - example exception Error: test to exception
          at Object.it (D:\dev\startse\packages\logger\test\logger.test.ts:56:19)
          at Object.asyncJestTest (D:\dev\startse\packages\logger\node_modules\jest-jasmine2\build\jasmineAsyncInstall.js:106:37)
          at resolve (D:\dev\startse\packages\logger\node_modules\jest-jasmine2\build\queueRunner.js:45:12)
          at new Promise (<anonymous>)
          at mapper (D:\dev\startse\packages\logger\node_modules\jest-jasmine2\build\queueRunner.js:28:19)
          at promise.then (D:\dev\startse\packages\logger\node_modules\jest-jasmine2\build\queueRunner.js:75:41)
          at process._tickCallback (internal/process/next_tick.js:68:7)
    */

   // usages

   try {
       const db = new Connection() // throw if fail connection
   }
   catch(e) {
       logger.exception(e)
   }
~~~
