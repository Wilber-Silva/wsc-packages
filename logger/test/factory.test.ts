import { ILogger } from '@wsc-company/types'
import { Factory } from '../src'

describe('Factory Test', () => {
  let logger: ILogger = null
  const { LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE } = process.env

  beforeEach(() => {
    logger = Factory.getLoggerLog4Js(LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE)
  })
  // 'Test instance of Logger'
  it('Test instance of Logger', () => {
    expect(logger).toBeDefined()
    // expect(logger).toBeInstanceOf<ILogger>(logger)
  })
})
