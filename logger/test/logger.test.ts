import { Factory } from './../src'
import { ILogger } from '@wsc-company/types'

describe('Logger Test', () => {
  let logger: ILogger = null

  const { LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE } = process.env

  beforeEach(() => {
    logger = Factory.getLogger(LOGGER_LEVEL, LOGGER_TYPE, LOGGER_FILE)
  })

  // Test logger has loaded
  it('Test logger has loaded', () => {
    expect(logger).toBeDefined()
  })

  // 'Test logger trace method'
  it('Test logger trace method', () => {
    const message = 'test to trace'
    const args = [{ param: 'trace' }]
    logger.trace(message, args)
  })

  // 'Test logger debug method'
  it('Test logger debug method', () => {
    const message = 'test to debug'
    const args = [{ param: 'debug' }]
    logger.debug(message, args)
  })

  // 'Test logger info method'
  it('Test logger info method', () => {
    const message = 'test to info'
    const args = [{ param: 'info' }]
    logger.info(message, args)
  })

  // 'Test logger warning method'
  it('Test logger warning method', () => {
    const message = 'test to warning'
    const args = [{ param: 'warning' }]
    logger.warning(message, args)
  })

  // 'Test logger error method'
  it('Test logger error method', () => {
    const message = 'test to error'
    const args = [{ param: 'error' }]
    logger.error(message, args)
  })

  // 'Test logger exception method'
  it('Test logger exception method', () => {
    const message = 'test to exception'
    const error = new Error(message)
    expect(error).toBeDefined()
    logger.exception(error)
  })

  // 'Test logger fatal method'
  it('Test logger fatal method', () => {
    const message = 'test to fatal'
    const args = [{ param: 'fatal' }]
    logger.fatal(message, args)
  })
})
