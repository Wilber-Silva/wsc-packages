import { BaseCache } from './BaseCache'
import { CacheRepository } from './CacheRepository'
import { Tedis } from 'tedis'

export class Factory {
  /**
   * Method to get cache provider
   *
   * @param host Custom host address
   * @param port Custom host port
   * @return Tedis
   */
  public static getCacheProvider(host?: string, port?: number): Tedis {
    return new Tedis({
      host: host || process.env.REDIS_HOST,
      port: port || 6379,
    })
  }
  /**
   * Method to get Cache Object
   *
   * @return BaseCache
   */
  public static getCache(): BaseCache {
    return new BaseCache(this.getCacheProvider())
  }
  /**
   * Method to get Cache Repository
   *
   * @return CacheRepository
   */
  public static getCacheRepository(): CacheRepository {
    return new CacheRepository(this.getCache())
  }
}
