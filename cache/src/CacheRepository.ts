import {
  ICacheRedis,
  ICacheRepository,
  IUserCacheSchema,
} from '@wsc/types'
export class CacheRepository implements ICacheRepository {
  key: string
  value: any

  /**
   * Method to construct instance of CacheRepository
   *
   * @param cache Cache Provider Instance
   * @param name Cache Provide Name
   * @return void
   */
  constructor(private cache: ICacheRedis, public name?: string) {}

  /**
   * Method save user on cache
   *
   * @param data User to save on cache
   * @return void
   */
  setUser(data: IUserCacheSchema): Promise<IUserCacheSchema> {
    return this.cache.set<IUserCacheSchema>(data.cpf, data)
  }
}
