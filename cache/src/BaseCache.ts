import { Tedis } from 'tedis'
import { ICacheRedis } from '@wsc/types'

type anything = any

export class BaseCache implements ICacheRedis {
  /**
   * Method to construct instance of Cache Object
   *
   * @param _cache Cache Provider Instance
   * @return void
   */
  constructor(private readonly _cache: Tedis) {}

  /**
   * Method set cache data
   *
   * @param key Id on cache data base
   * @param value Body to save on cache
   * @return T
   */
  async set<T>(key: string, value: anything): Promise<T> {
    let data = ''
    const old = await this.get<T>(key)
    if (typeof value === 'string') data = value
    else if (typeof value === 'object')
      data = this.toString({ ...old, ...value })

    return this._cache.set(key, data)
  }
  /**
   * Method get cache data
   *
   * @param key Id on cache data base
   * @return T
   */
  async get<T>(key: string): Promise<T> {
    const str = await this._cache.get(key)
    return this.toObject<T>(str)
  }
  /**
   * Method convert to string
   *
   * @param value Object to convert to string
   * @return string
   */
  toString<T>(value: T): string {
    return JSON.stringify(value)
  }
  /**
   * Method convert to json
   *
   * @param cache Cache data to convert to object
   * @return T
   */
  toObject<T>(cache: string | number): T {
    if (typeof cache === 'string') return JSON.parse(cache)
    return null
  }
}
