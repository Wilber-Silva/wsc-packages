import { Factory, GroupsSchema } from './../src'

describe('Groups', () => {
  const groups = Factory.getGroupSchema({})
  it('check type instance', () => {
    expect(groups).toBe(GroupsSchema)
  })
})
