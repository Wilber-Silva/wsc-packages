import { Factory, UsersSchema } from './../src'

describe('Users', () => {
  const users = Factory.getUserSchema({})
  it('check type instance', () => {
    expect(users).toBe(UsersSchema)
  })
})
