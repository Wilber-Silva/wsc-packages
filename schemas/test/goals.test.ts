import { Factory, GoalsSchema } from './../src'

describe('Goals', () => {
  const goals = Factory.getGoalsSchema({})
  it('check type instance', () => {
    expect(goals).toBe(GoalsSchema)
  })
})
