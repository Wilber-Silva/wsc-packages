module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>/test'],
  transform: {
    '^.+\\.test.ts?$': 'ts-jest'
  },
  coverageThreshold: {
    global: {
        branches: 1,
        functions: 1,
        lines: 1,
        statements: 1
    }
  },
  coverageReporters: ['json', 'lcov', 'text', 'clover']
};
/*
module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  roots: ['<rootDir>/test'],
  transform: {
      '^.+\\.tsx?$': 'ts-jest'
  },
  coverageThreshold: {
      global: {
          branches: 1,
          functions: 1,
          lines: 1,
          statements: 1
      }
  },
  coverageReporters: ['json', 'lcov', 'text', 'clover']
};
*/