import { IGroupsSchema } from '@wsc-company/types'
import { Timestamp } from './firestore'

export class GroupsSchema implements IGroupsSchema {
  _id?: string
  name?: string
  tag?: string
  createdAt?: Timestamp
  updatedAt?: Timestamp
  deletedAt?: Timestamp
  constructor(fields: IGroupsSchema) {
    Object.assign(this, fields)
  }
}

export default GroupsSchema
