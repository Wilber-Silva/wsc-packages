import {
  IGoalsSchema,
  IGroupsSchema,
  ITransactionCoinsSchema,
  IUserSchema,
} from '@wsc-company/types'
import {
  GoalsSchema,
  GroupsSchema,
  TransactionCoinsSchema,
  UsersSchema,
} from '.'

export class Factory {
  /**
   * Method to get a goals schema instance
   * @param params IGoals interface
   */
  public static getGoalsSchema(params: IGoalsSchema): IGoalsSchema {
    return new GoalsSchema(params)
  }

  /**
   * Method to get a groups schema instance
   * @param params IGroups interface
   */
  public static getGroupSchema(params: IGroupsSchema): IGroupsSchema {
    return new GroupsSchema(params)
  }

  /**
   * Method to get a user schema instance
   * @param params IUser interface
   */
  public static getUserSchema(params: IUserSchema): IUserSchema {
    return new UsersSchema(params)
  }

  /**
   * Method to get a transaction coins schema instance
   * @param params ITransactionCoins interface
   */
  public static getTransactionCoinsSchema(
    params: ITransactionCoinsSchema,
  ): ITransactionCoinsSchema {
    return new TransactionCoinsSchema(params)
  }
}
