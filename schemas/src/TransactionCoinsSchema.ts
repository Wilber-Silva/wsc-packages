import {
  COINS_TYPE_OPERATION,
  ITransactionCoinsSchema,
} from '@wsc-company/types'
import { Timestamp } from './firestore'

export class TransactionCoinsSchema implements ITransactionCoinsSchema {
  description: string
  coins: number
  type: COINS_TYPE_OPERATION
  active: boolean
  userId: string
  expiredAt: string | Date
  _id?: string
  createdAt?: Timestamp
  updatedAt?: Timestamp
  deletedAt?: Timestamp

  constructor(params: ITransactionCoinsSchema) {
    Object.assign(this, params)
  }
}
