export type Timestamp = Date | string

export class BaseSchemaFirestore {
  _id?: string
  createdAt?: Timestamp | string
  updatedAt?: Timestamp | string
  deletedAt?: Timestamp | string

  constructor(fields: {
    _id?: string
    updatedAt?: Timestamp | string
    deletedAt?: Timestamp | string
    createdAt?: Timestamp | string
  }) {
    this._id = fields._id
    this.createdAt = fields.createdAt || null
    this.updatedAt = fields.updatedAt || null
    this.deletedAt = fields.deletedAt || null
  }
}
