import { IUserSchema } from '@wsc-company/types'
import { Timestamp } from './firestore'

export class UsersSchema implements IUserSchema {
  _id?: string
  cpf?: string
  points?: number
  active?: boolean
  groupId?: string
  currentGoalId?: string
  createdAt?: Timestamp
  updatedAt?: Timestamp
  deletedAt?: Timestamp

  constructor(fields: IUserSchema) {
    Object.assign(this, fields)
  }
}
