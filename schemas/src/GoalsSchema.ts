import { Timestamp } from './firestore'
import { IGoalsSchema } from '@wsc-company/types'

export class GoalsSchema implements IGoalsSchema {
  _id?: string
  points?: number
  coins?: number
  order?: number
  name?: string
  groupId?: string
  active?: boolean
  publishAt?: Date
  expiredAt?: Date
  createdAt?: Timestamp
  updatedAt?: Timestamp
  deletedAt?: Timestamp

  constructor(fields: IGoalsSchema) {
    Object.assign(this, fields)

    let publishAt
    let expiredAt

    if (typeof fields.publishAt === 'string')
      publishAt = new Date(fields.publishAt)
    if (typeof fields.expiredAt === 'string')
      expiredAt = new Date(fields.expiredAt)
    this.publishAt = publishAt
    this.expiredAt = expiredAt
  }
}
